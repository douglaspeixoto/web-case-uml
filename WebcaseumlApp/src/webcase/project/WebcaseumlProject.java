package webcase.project;

import java.util.List;

import webcase.entities.uml.classdiagram.UMLClassDiagram;
import webcase.entities.uml.classdiagram.profile.UMLProfile;

/**
  * The project itself, with its diagrams and profiles.
  * Every diagram or profile of the project must be
  * added in a instance of this class. One instance for
  * each project.
  * 
  * @author Douglas Alves Peixoto
  */
public class WebcaseumlProject {
	private List<UMLClassDiagram> classDiagrams;
	private List<UMLProfile> profiles;
	/*declaration of possible future diagrams*/
	
	
}
