package webcase.client.workspace;

import java.util.Arrays;
import java.util.List;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * Palette Tab of the workspace. 
 * {@link StackLayoutPanel} widget.
 * 
 * @author Douglas Alves Peixoto
 */

/* depois tem q mudar o EntryPoint.. esta calsse nao eh a entry point
*  Entry point eh a classe q inicia o programa (main) neste caso vai ser
*  a workspace ou desktop.... mudar tbm o nome da fun��o onModuleLoad
*  para onInitialize.
*  
*  public class PaletteTab extends ContentWidget{
*  		public Widget onInitialize() {
*  		}
*  }
*/


public class PaletteTab implements EntryPoint {
	private/* static */StackLayoutPanel paletteTab; // The palette itself
	private/* static */Images images;

	private/* static */List<PaletteTabItem> classDiagramItems;
	private/* static */List<PaletteTabItem> profileItems;

	private/* static */SelectionModel<PaletteTabItem> selectionModel;

	/**
	 * Define a key provider for a PaletteTabItem. We use the unique ID as the
	 * key, which allows to maintain selection even if the name changes.
	 */
	private ProvidesKey<PaletteTabItem> keyProvider = new ProvidesKey<PaletteTabItem>() {
		@Override
		public Object getKey(PaletteTabItem item) {
			// Always do a null check.
			return (item == null) ? null : item.getId();
		}
	};

	/**
	 * A custom {@link Cell} used to render a {@link PaletteTabItem}. 
	 * Cell of the CellList.
	 */
	private static class ItemCell extends AbstractCell<PaletteTabItem> {
		@Override
		public void render(Context context, PaletteTabItem item,
				SafeHtmlBuilder sb) {
			if (item != null) {
				// The HTML of the image used for palette.
				String imageHtml = AbstractImagePrototype
						.create(item.getIcon()).getHTML();

				sb.appendHtmlConstant("<table>");
				// Add the tab item icon.
				sb.appendHtmlConstant("<tr><td rowspan='3'>");
				sb.appendHtmlConstant(imageHtml);
				sb.appendHtmlConstant("</td>");
				// Add the name of the item.
				sb.appendHtmlConstant("<td style='font-size:95%;'>");
				sb.appendEscaped(item.getItemName());
				sb.appendHtmlConstant("</td></tr></table>");
			}
		}
	}

	/**
	 * Entry point method.
	 */
	public void onModuleLoad() {
		// create a new stack layout panel.
		paletteTab = new StackLayoutPanel(Unit.EM);
		// get the images (icons used in the palette)
		images = Images.INSTANCE; //(Images) GWT.create(Images.class);

		// sets the initial size of the tab
		paletteTab.setPixelSize(200, 400);
		
		// creates the header widget to the classDiagram items
		Widget classDiagramHeader = createHeaderWidget("Class Diagram",
				images.classDiagramHeader());
		// adds the classDiagram and header widgets to the palette
		paletteTab.add(classDiagramItems(), classDiagramHeader, 5);
		
		// creates the header widget to the profile items 
		Widget profileHeader = createHeaderWidget("Profile",
				images.profileHeader()); 
		// adds the profile and header widgets to the palette 
		paletteTab.add(profileItems(), profileHeader, 5);

		/* add other future palette tab items here */

		// Add the widgets to the root panel.
		RootPanel.get().add(paletteTab);
	}

	/**
	 * Creates a widget to display in the header of each tab that includes an
	 * image and some text.
	 * 
	 * @param Text the header text
	 * @param Image the {@link ImageResource} to add next to the header
	 * @return The header widget
	 */
	private/* static */Widget createHeaderWidget(String text, ImageResource image) {
		// Add the image and text to a horizontal panel
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setHeight("100%");
		hPanel.setSpacing(0);
		hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		hPanel.add(new Image(image));
		HTML headerText = new HTML(text);
			 headerText.setStyleName("cw-StackPanelHeader"); // css
		hPanel.add(headerText);

		return new SimplePanel(hPanel);
	}

	/**
	 * Create the {@link CellList} of Class Diagram options.
	 * 
	 * @return The {@link CellList} of Class Diagram options.
	 */
	private/* static */Widget classDiagramItems() {
		// The list of class diagram items to display.
		// Sets the item name, icon and ID
		classDiagramItems = Arrays
				.asList(new PaletteTabItem("Class", images.classIcon(), 1),
						new PaletteTabItem("Relationship", images.relationshipIcon(), 2),
						new PaletteTabItem("Package", images.packageIcon(), 3),
						new PaletteTabItem("Data Type", images.datatypeIcon(), 4),
						new PaletteTabItem("Enumeration", images.enumerationIcon(), 5)
						/* Add new class diagram elements here */
				);

		// Create a CellList using the keyProvider.
		CellList<PaletteTabItem> cellList = new CellList<PaletteTabItem>(
				new ItemCell(), keyProvider);

		// Push data into the CellList.
		cellList.setRowCount(classDiagramItems.size(), true);
		cellList.setRowData(0, classDiagramItems);
		// Set the number of rows per page and refresh the view
		cellList.setPageSize(10);
		// Determines how keyboard paging will work
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		// Determines how keyboard selection will work.
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model using the same keyProvider,
		// so we can select cells.
		selectionModel = new SingleSelectionModel<PaletteTabItem>(keyProvider);
		cellList.setSelectionModel(selectionModel);

		// Creates the event when select an item
		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						// TODO
						// informar a aplica��o qual item foi seleccionado
						// para saber qual objeto deve ser desenhado
						Window.alert("Clicaste em uma linha da class diagram, percebes?");
					}
				});

		return cellList;
	}

	/**
	 * Create the {@link CellList} of Profile options.
	 * 
	 * @return The {@link CellList} of Profile options.
	 */
	private/* static */Widget profileItems() {
		// The list of class diagram items to display.
		// Sets the item name, icon and ID
		profileItems = Arrays
				.asList(new PaletteTabItem("Stereotype", images.stereotypeIcon(), 1),
						new PaletteTabItem("Relationship", images.relationshipIcon(), 2),
						new PaletteTabItem("Package", images.packageIcon(), 3),
						new PaletteTabItem("Data Type", images.datatypeIcon(), 4),
						new PaletteTabItem("Enumeration", images.enumerationIcon(), 5)
						/* Add new class diagram elements here */
				);

		// Create a CellList using the keyProvider.
		CellList<PaletteTabItem> cellList = new CellList<PaletteTabItem>(
				new ItemCell(), keyProvider);

		// Push data into the CellList.
		cellList.setRowCount(profileItems.size(), true);
		cellList.setRowData(0, profileItems);
		// Set the number of rows per page and refresh the view
		cellList.setPageSize(10);
		// Determines how keyboard paging will work
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		// Determines how keyboard selection will work.
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model using the same keyProvider,
		// so we can select cells.
		selectionModel = new SingleSelectionModel<PaletteTabItem>(keyProvider);
		cellList.setSelectionModel(selectionModel);

		// Creates the event when select an item
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						// TODO
						// informar a aplica��o qual item foi seleccionado
						// para saber qual objeto deve ser desenhado
						Window.alert("Clicaste em uma linha do profile, percebes?");
					}
				});

		return cellList;
	}

	/**
	 * Select a PalleteTabItem. The selectionModel will select based on the ID
	 * because we used a keyProvider.
	 * 
	 * @param Item ID.
	 * @return A palette tab item.
	 */
	public void selectItemById(int id) {
		/*
		 * PalleteTabItem item = classDiagramItems.get(id+1);
		 * selectionModel.setSelected(item, true);
		 * 
		 * /* Select a contact. The selectionModel will select based on the ID
		 * because we used a keyProvider.
		 */
		/*
		 * Contact sarah = CONTACTS.get(3); selectionModel.setSelected(sarah,
		 * true);
		 * 
		 * // Modify the name of the contact. sarah.name = "Sara";
		 * 
		 * /* Redraw the CellList. Sarah/Sara will still be selected because we
		 * identify her by ID. If we did not use a keyProvider, Sara would not
		 * be selected.
		 */
		/*
		 * cellList.redraw();
		 * 
		 * return item;
		 */
	}

	/**
	 * Returns a instance of the palette tab.
	 * 
	 * @return Pallete Widget {@link StackLayoutPanel}
	 */
	/*
	 * public static final Widget getPalleteTab(){ onInitialize();
	 * 
	 * return palleteTab; }
	 */

}
