package webcase.client.workspace;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * This class represents the workspace itself.
 * It creates the Widget of the whole workspace, 
 * with all its tabs. All the tabs were created 
 * separately.
 * 
 * It is a {@link SplitLayoutPanel} widget
 * inside of a {@link VerticalPanel} widget.
 * 
 * @author Douglas Alves Peixoto
 */
public class Workspace implements EntryPoint {
	private VerticalPanel workspace;
	private SplitLayoutPanel splitPanel;
	
	// {@link MenuTab}
	// declarar as tbs aqui
	// private static MenuTab menuTab;
	
	@Override
	public void onModuleLoad() {
	    // Create the Split Panel, which will receive the Tabs
	    splitPanel = new SplitLayoutPanel(5);
	    // Sets the element ID, for debug reasons
	    splitPanel.ensureDebugId("workspaceSplitLayoutPanel");
	    // Set some properties
	    splitPanel.getElement().getStyle()
	        .setProperty("border", "3px solid #e7e7e7");

	    // Add the Widgets (tabs) to the Split Panel.
	    // It has to be fitted in this order
	    /*
 * splitPanel.addWest(projectExplorerTab, 50);
 * splitPanel.addSouth(propertiesTab, 50);
 * splitPanel.addEast(paletteTab, 50);
 * splitPanel.add(designPanel, 100);
	     *
	     * // caso queire adicionar o painel com Scroll 
	     * // Add scrollable text to the center.
 * splitPanel.add(new ScrollPanel(designPanel), 100);
	     */
	
		// Creates a new vertical panel (the main panel).
	    workspace = new VerticalPanel();
	    // Sets the space between the cells
	    workspace.setSpacing(5);
	    
	    // The first row of the panel (workspace) is the Menu Tab,
	    // it is not a split panel.
//workspace.add(menuTab);
	    
	    // The second row of the panel is the SplitLayoutPanel
	    workspace.add(splitPanel);
	    
	    // Return the workspace
	    workspace.ensureDebugId("workspaceVerticalPanel");
	    
	    // Add the workspace to the root panel.
		RootPanel.get().add(workspace);
	}
}
