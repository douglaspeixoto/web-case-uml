package webcase.client.workspace;

/**
 * A simple data type that represents a 
 * palette item, its name, icon (image) and ID.
 * 
 * @author Douglas Alves Peixoto
 */
import com.google.gwt.resources.client.ImageResource;

class PaletteTabItem {
	private final int id;
	private final String itemName;
	private final ImageResource icon;

	/**
	 * Default constructor.
	 * 
	 * @param Item name.
	 * @param Item icon.
	 */
	public PaletteTabItem(String itemName, ImageResource icon, int id) {
		this.id = id;
		this.itemName = itemName;
		this.icon = icon;
	}

	/**
	 * Return the palette tab item ID.
	 * 
	 * @return Item ID.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Return the palette tab item name.
	 * 
	 * @return Item name.
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * Returns the palette tab item icon.
	 * 
	 * @return Item icon.
	 */
	public ImageResource getIcon() {
		return icon;
	}
}
