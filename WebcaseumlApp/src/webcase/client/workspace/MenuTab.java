package webcase.client.workspace;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;

/**
 *  Menu Tab of the workspace. 
 * {@link MenuBar} widget.
 * 
 * @author Douglas Alves Peixoto
 */
public class MenuTab implements EntryPoint {
	private MenuBar menuTab;
	
	// Menus
	private MenuBar fileMenu;
	private MenuBar editMenu;
	
	// SubMenus
	private MenuBar newSubMenu;
	private MenuBar exportSubMenu;
	
	@Override
	public void onModuleLoad() {
	    // Create the menu tab
	    menuTab = new MenuBar();
	    menuTab.setAutoOpen(true);
	    menuTab.setWidth("500px");
	    menuTab.setAnimationEnabled(true);
	    
	    // Add the menus
	    menuTab.addItem(new MenuItem("File", getFileMenu()));
	    menuTab.addItem(new MenuItem("Edit", getEditMenu()));

	    // Return the menu
	    menuTab.ensureDebugId("MenuTab");		
	}
	
	/**
	 * Creates the File menu
	 * 
	 * @return menu item
	 */
	private MenuBar getFileMenu(){
	    // Create the file menu
	    fileMenu = new MenuBar(true);
	    fileMenu.setAnimationEnabled(true);
	    
	    // Add the options
	    fileMenu.addItem("New", getNewSubMenu());
	    fileMenu.addItem("Save", menuCommand);
	    fileMenu.addItem("Export", getExportSubMenu());
	    fileMenu.addSeparator();
	    fileMenu.addItem("Exit", menuCommand);

	    return fileMenu;
	}
	
	/**
	 * Creates the Edit menu
	 * 
	 * @return menu item
	 */
	private MenuBar getEditMenu(){
	    // Create the Edit menu
	    editMenu = new MenuBar(true);
	    editMenu.setAnimationEnabled(true);
	    
	    // Add the options
	    editMenu.addItem("Undo", menuCommand);
	    fileMenu.addItem("Rendo", menuCommand);

	    return fileMenu;
	}
	
	/**
	 * Creates the New sub menu
	 * 
	 * @return menu item
	 */
	private MenuBar getNewSubMenu(){
	    // Create the new sub menu
		newSubMenu = new MenuBar(true);
		newSubMenu.setAnimationEnabled(true);
	    
	    // Add the options
		newSubMenu.addItem("Class Diagram", menuCommand);
		newSubMenu.addItem("Profile", menuCommand);
		newSubMenu.addItem("other menu", menuCommand);
	    
	    return newSubMenu;
	}

	/**
	 * Creates the Export sub menu
	 * 
	 * @return menu item
	 */
	private MenuBar getExportSubMenu(){
	    // Create the Export sub menu
		exportSubMenu = new MenuBar(true);
		exportSubMenu.setAnimationEnabled(true);
	 
	    // Add the options
		exportSubMenu.addItem("XMI Format", menuCommand);
	    
	    return exportSubMenu;
	}
	
	// Create the commands that will execute on menu item selection
    private Command menuCommand = new Command() {
      public void execute() {
        Window.alert("N�o faz nada ainda.");
      }
    };
}
