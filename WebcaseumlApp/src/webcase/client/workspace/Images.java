package webcase.client.workspace;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Specifies the images used in 
 * this application.
 * 
 * @author Douglas Alves Peixoto
 */
public interface Images extends ClientBundle{
	/**
	 * Class images instance
	 */
	Images INSTANCE = GWT.create(Images.class);
	
	// Palette header icons
	@Source("classDiagramHeader.png")
	ImageResource classDiagramHeader();
	@Source("profileHeader.png")
	ImageResource profileHeader();
	
	// Class diagram e profile objects icons
	@Source("classIcon.png")
	ImageResource classIcon();
	@Source("relationshipIcon.png")
	ImageResource relationshipIcon();
	@Source("generalizationIcon.png")
	ImageResource generalizationIcon();
	@Source("dataTypeIcon.png")
	ImageResource datatypeIcon();
	@Source("enumerationIcon.png")
	ImageResource enumerationIcon();
	@Source("packageIcon.png")
	ImageResource packageIcon();
	@Source("stereotypeIcon.png")
	ImageResource stereotypeIcon();
}
