package webcase.entities.uml;

/**
 * This enumeration contains the name of all UML 
 * elements used in this program.
 * If a new UML element is added in the program, 
 * its name must be added to this enumeration exactly
 * how it is declared in the class name.
 * 
 * It is used to compare the type of an UML element.
 * 
 * @author Douglas Alves Peixoto
 */
public enum UMLElementsName {
	UMLAttribute,
	UMLOperation,
	UMLClass,
	UMLDataType,
	UMLEnumeration,
	UMLPackage,
	UMLRelationship,
	UMLStereotype;
}
