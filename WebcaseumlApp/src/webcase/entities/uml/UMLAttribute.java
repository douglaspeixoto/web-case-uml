package webcase.entities.uml;

import webcase.entities.uml.classdiagram.profile.UMLMultiplicity;
import webcase.entities.uml.classdiagram.profile.UMLVisibility;

/**
 * Entity of the element UMLAttribute.
 * Contains its properties.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLAttribute extends UMLElement {
	private UMLElement type;
	private UMLVisibility visibility;
	private UMLMultiplicity multiplicity;

	/**
      * Default constructor.
      * Sets the attribute name.
	  * Sets the attribute visibility as PRIVATE.
	  * Sets the attribute multiplicity as ZERO_TO_ONE.
	  * @param Attribute name.
	  */
	public UMLAttribute(String name) {
		setName( name );
		visibility = UMLVisibility.PRIVATE;
		multiplicity = UMLMultiplicity.ZERO_TO_ONE;
	}

	/**
      * Return the attribute type.
	  * @return Attribute type - can be any UMLElement.
	  */
	public UMLElement getType() {
		return type;
	}

	/**
      * Returns the attribute type name.
	  * @return Name of the attribute type name.
	  */
	public String getTypeName() {
		return type.getName();
	}
	
    /**
      * Returns the attribute visibility.
	  * @return Enumeration (PUBLIC, PRIVATE or PROTECTED).
	  */
	public UMLVisibility getVisibility() {
		return visibility;
	}

    /**
      * Returns the attribute lower bound,
      * can be ZERO, ONE or MANY.
	  * @return 0, 1 or -1 (to many).
	  */
	public int getLowerBound() {
		if ( multiplicity == UMLMultiplicity.ZERO_TO_ONE || multiplicity == UMLMultiplicity.ZERO_TO_MANY )
			return 0;
		else if ( multiplicity == UMLMultiplicity.ONE_TO_ONE || multiplicity == UMLMultiplicity.ONE_TO_MANY )
			return 1;
		else
			return -1;
	}

    /**
      * Returns the attribute upper bound,
      * can be ONE or MANY.
	  * @return 1 or -1 (for many).
	  */
	public int getUpperBound() {
		if ( multiplicity == UMLMultiplicity.ZERO_TO_ONE || multiplicity == UMLMultiplicity.ONE_TO_ONE )
			return 1;
		else
			return -1;
	}

	/**
      * Sets the attribute data type.
      * In this case, you have to pass the object 
      * that refers to the type itself.
      * @param Type of the attribute - UML element.
	  */
	public void setType(UMLElement type) {
		this.type = type;
	}

	/**
      * Sets the attribute visibility.
      * @param UMLVisibility - PUBLIC, PRIVATE or PROTECTED.
	  */
	public void setVisibility(UMLVisibility visibility) {
		this.visibility = visibility;
	}

	/**
      * Sets the attribute multiplicity.
      * @param Enumeration.
	  */
	public void setMultiplicity(UMLMultiplicity multiplicity) {
		this.multiplicity = multiplicity;
	}
	
}


/*
 * "Dei um trocado pra um pivete no farol,
 * olhei pro lado tava o pai, pensei:
 * Velho filho da puta, explorador.
 * Mas vai saber, sei l�...
 * Cada um tem sua hist�ria, 
 * eu to aqui pra aprender, 
 * n�o pra julgar,
 * quem pode me julgar?
 * Pelo menos desde cedo o pivete vai aprender a se virar."
 * 
 */