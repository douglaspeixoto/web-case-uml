package webcase.entities.uml;

import java.util.List;

/**
 * This interface contains the basic functions  
 * for any class which contains a list of 
 * UMLAttribute as parameter.
 * 
 * @author Douglas Alves Peixoto
 */
public interface TreatAttributes {
	public List<UMLAttribute> getAttributes() throws Exception;
	public UMLAttribute getAttributeByName(String name) throws Exception;
	public boolean addAttribute(UMLAttribute attr) throws Exception;
	public boolean removeAttribute(UMLAttribute attr) throws Exception;
	public int numberOfAttributes();
}
