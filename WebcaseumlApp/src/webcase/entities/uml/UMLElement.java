package webcase.entities.uml;

import java.util.ArrayList;
import java.util.List;

import webcase.entities.uml.classdiagram.profile.UMLMetaclass;
import webcase.entities.uml.classdiagram.profile.UMLStereotype;

/**
 * Entity of the UMLElement. 
 * All entities of UML elements must extends this class,
 * directly or indirectly.
 * 
 * @author Douglas Alves Peixoto
 */
public abstract class UMLElement {
	private String name;
	private List<UMLStereotype> appliedStereotypes;	
	private static final List<UMLMetaclass> metaclasses = 
		UMLMetaclass.getMetaclasses();
	
	/**
      * Default Constructor.
      * Sets the element name as an empty string.
	  */
	public UMLElement() {
		name = new String("");	
		appliedStereotypes = null;
	}
	
	/**
      * Returns the element name.
	  * @return Element name.
	  */
	public String getName() {			 
		return this.name;
	}
	
	/**
      * Returns the stereotypes applied to the element.
	  * @return List of stereotypes.
	  * @throws	Throws exception if there is no stereotype
	  * 		applied to the element ("ThereIsNoStereotypeApplied!").
	  */
	public List<UMLStereotype> getAppliedStereotypes() throws Exception {
		if (appliedStereotypes == null || appliedStereotypes.isEmpty()){
			throw new Exception("ThereIsNoStereotypeApplied!");
		}
		return appliedStereotypes;
	}
	
	/**
      * Sets the element name.
      * @param Element name.
      * @return Returns false if the element name is not valid,
      * 		it can contains only letters,numbers or underline.   
      * 		Can not be blank or start with number.
      * 		Otherwise returns true.
	  */
	public boolean setName(String name) {
		// name must be between 1 and 20 chars that are numbers, letters or underline.
	    if ( !name.matches("^[0-9a-zA-Z\\_]{1,20}$") ) {
	    	return false;
	    }
	    // name must not start with number.
	    if( name.substring(0,1).matches("[0-9]") ) {
	    	return false;
	    }
	    
	    this.name = name;
	    
		return true;
	}

	/**
      * Apply a stereotype to this element.
      * @param Stereotype.
      * @return Returns true if it has been inserted successfully.
      * 		Returns false if stereotypes cannot be applied to 
      * 		elements of its type.
	  * @throws Throws exception if the UML element already has
      * 		this stereotype applied (StereotypeAlreadyApplyed!).
      * @throws Throws exception if the UML element already has a
      * 		stereotype with this name (StereotypeNameAlreadyApplyed!).
	  */
	// Hereafter should be verified if the stereotype can be applied,
	// through OCL constraints, call the method of the profile which validates
	// the stereotype.
	public boolean applyStereotype(UMLStereotype stereotype) throws Exception {
		// Verify if stereotypes can be applied in this kind of element.
		for( UMLMetaclass meta : metaclasses ){
			if( meta.name() == stereotype.getClass().getName() ){
				if( appliedStereotypes == null ){
					appliedStereotypes = new ArrayList<UMLStereotype>();
				}
				
				// Throws exception if it's already in the element.
				if( appliedStereotypes.contains( stereotype ) ){
					throw new Exception("StereotypeAlreadyApplyed!"); 
				}
				// Throws exception if it's already has a stereotype 
				// with this name
				for( UMLStereotype elem : appliedStereotypes ){
					if( stereotype.getName() == elem.getName() ){
						throw new Exception("StereotypeNameAlreadyApplyed!");
					}
				}
				
				appliedStereotypes.add( stereotype );
				
				return true;
			}
		}
		/*if( !metaclasses.contains( this.getClass().getName() ) )
			//throw new Exception("CannotApplyStereotype!"); 
		
		if( appliedStereotypes == null )
			appliedStereotypes = new ArrayList<UMLStereotype>();
		
		// Throws exception if it's already in the element.
		if( appliedStereotypes.contains( stereotype ) )
			throw new Exception("StereotypeAlreadyApplyed!"); 
		for( UMLStereotype elem : appliedStereotypes )
			if( stereotype.getName() == elem.getName() )
				throw new Exception("StereotypeNameAlreadyApplyed!");
		
		appliedStereotypes.add( stereotype );*/
		
		return false;
	}

	/**
      * Removes a stereotype applied to the element.
      * @param Streotype to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if it has no stereotype applied
      * 		(ThereIsNoStereotypeApplied!).
      * @throws	Throws exception if there is not the element to be
      * 		removed (ThereIsNotSuchStereotypeApplied!).
	  */
	public boolean removeAppliedStereotype(UMLStereotype stereotype) throws Exception {
		if ( appliedStereotypes == null || appliedStereotypes.isEmpty() ){
			throw new Exception("ThereIsNoStereotypeApplied!");
		}
		else if ( !appliedStereotypes.remove( stereotype ) ){
			throw new Exception("ThereIsNotSuchStereotypeApplied!");
		}
		return true;
	}
	
	/**
      * Removes a stereotype applied to the element.
      * @param Name of the stereotype to be removed.
      * @return Returns true if it has been successfully removed,
      * 		returns false if it was not removed or did not find.
      * @throws	Throws exception if it has no stereotype applied
      * 		(ThereIsNoStereotypeApplied!).
      * @throws	Throws exception if occur some problem in remove
      * 		the stereotype (ThereIsNotSuchStereotypeApplied!).
	  */
	public boolean removeAppliedStereotypeByName(String name) throws Exception {
		if ( appliedStereotypes == null || appliedStereotypes.isEmpty() ){
			throw new Exception("ThereIsNoStereotypeApplied!");
		}
		for ( UMLStereotype elem : appliedStereotypes){
			 if( elem.getName() == name ){
				 if ( !appliedStereotypes.remove( elem ) ){
					throw new Exception("ErroToRemoveStereotypeApplied!");
				 }
				 else {
					return true;
				 }
			}
		}
		return false;		
	}
	
	/**
	  * Returns the stereotype named as 'name'.
	  * @return UML stereotype.
	  * @throws Throws exception if the element has not a stereotype
	  * 		named as 'name' (ThereIsNotSuchStereotype!).
	  */
	public UMLStereotype getStereotypeByName(String name) throws Exception{
		for( UMLStereotype elem : appliedStereotypes ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchStereotype!");
	}
}
