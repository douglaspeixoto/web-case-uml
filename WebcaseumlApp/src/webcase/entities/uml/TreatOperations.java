package webcase.entities.uml;

import java.util.List;

/**
 * This interface contains the basic functions  
 * for any class which contains a list of
 * UMLOperation as parameter.
 * 
 * @author Douglas Alves Peixoto
 */
public interface TreatOperations {
	public List<UMLOperation> getOperations() throws Exception;		
	public UMLOperation getOperationByName(String name) throws Exception;
	public boolean addOperation(UMLOperation operation) throws Exception;
	public boolean removeOperation(UMLOperation op) throws Exception;
	public int numberOfOperations();
}
