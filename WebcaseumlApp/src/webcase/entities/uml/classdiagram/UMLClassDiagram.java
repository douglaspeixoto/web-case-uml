package webcase.entities.uml.classdiagram;

import java.util.ArrayList;
import java.util.List;

import webcase.entities.uml.UMLElementsName;

/**
 * Entity of the element UMLClassDiagram.
 * Contains its properties and informations,
 * name of the diagram and its elements.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLClassDiagram {
	private String name;
	private List<UMLClassDiagramElement> classDiagramElements;

	/**
      * Default constructor.
	  */
	public UMLClassDiagram() {
		name = new String("");
		classDiagramElements = null;
	}

	/**
      * Returns the class diagram name.
	  * @return Class diagram name.
	  */
	public String getName() {
		return name;
	}

	/**
      * Returns a list of class diagram elements.
	  * @return List of class diagram elements.
	  * @throws	Throws exceptions if there is not elements 
	  * 		in the class diagram (ThereIsNoElement!).
	  */
	public List<UMLClassDiagramElement> getClassDiagramElements() throws Exception {
		if ( classDiagramElements == null || classDiagramElements.isEmpty() ){
			throw new Exception("ThereIsNoElement!");
		}
		return classDiagramElements;
	}
	
	/**
      * Sets the class diagram name.
      * @param Diagram name.
      * @return Returns false if the name is invalid,
      * 		can not start with number.
      */	
	public boolean setName(String name) {
	    // name must not start with number.
	    if( name.substring(0,1).matches("[0-9]") ){
	    	return false;
	    }
	  	    
		this.name = name;
		
		return true;
	}	
	
	/**
      * Add an UML element to the class diagram.
      * @param Class diagram element to be added.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the class diagram already has 
      * 		the element (ElementAlreadyAdded!).
      * @throws Throws exception if the class diagram already has a element
      * 		of the same type with the same name (ElementNameAlreadyExists!).
	  */	
	public boolean addClassDiagramElement(UMLClassDiagramElement element) throws Exception {
		if ( classDiagramElements == null ){
			classDiagramElements = new ArrayList<UMLClassDiagramElement>();
		}
		
		// Throws exception if it's already in the class diagram.
		if( classDiagramElements.contains( element ) ){
			throw new Exception("ElementAlreadyAdded!");
		}
		// Throws exception if exists an object of the same type (Class name)
		// and same name. 
		for( UMLClassDiagramElement elem : classDiagramElements ){
			if( element.getClass().getName() == elem.getClass().getName() && 
				element.getName() == elem.getName() ){
				throw new Exception("ElementNameAlreadyExists!");
			}
		}
		
		classDiagramElements.add( element );
		
		return true;
	}

	/**
      * Removes an element of the diagram.
      * @param Diagram element to be deleted.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exceptions if the diagram has no element 
      * 		(ThereIsNotElements!).
      * @throws	Thows exception if the diagram has not the element
      * 		to be deleted (ThereIsNotSuchElement!).
	  */
	public boolean removeClassDiagramElement(UMLClassDiagramElement elem) throws Exception {
		if ( classDiagramElements == null || classDiagramElements.isEmpty() ){
			throw new Exception("ThereIsNotElements!");
		}
		else if ( !classDiagramElements.remove( elem ) ){
			throw new Exception("ThereIsNotSuchElement!");
		}
		
		return true;
	}
	
	// Objects name change must be done through class diagram.
	/**
      * Change class diagram element name.
      * @param Diagram element which the name will be changed.
      * @param New element name.
      * @return Returns true if it has been successfully changed.
      * @throws	Throws exception if the diagram already has an element
      * 		of the same type with the same name (NameAlreadyExists!).
	  */	
	public boolean changeElementName(UMLClassDiagramElement element, String newName) throws Exception{
		if( hasDuplicateName(element, newName) ){
			throw new Exception("NameAlreadyExists!");
		}
		
		element.setName( newName );
		
		return true;
	}

	/**
      * Verify is there is some element with the same type of 'element'
      * with the same name (if the name is doubled).
      * @param Diagram element to be searched its type.
      * @param Name of the element to be searched.
      * @return Returns true if there is another element of the same
      * 		type of 'element' with the same name, false otherwise.
	  */
	public boolean hasDuplicateName(UMLClassDiagramElement element, String name){		
		for(UMLClassDiagramElement elem : classDiagramElements){
			if( elem.getClass().getName() == element.getClass().getName() &&
				elem.getName() == name && !elem.equals( element ) ){
				return true;
			}
		}
		
		return false;
	}

	/**
      * Count the number of occurrence of the element typed as the same type of 
      * 'element' in the class diagram.
      * @param Diagram element which will count the number 
      * 	   of elements of its type.
      * @return Number of elements with the same type of the element 'element'.
	  */
	public int numberOfElementsByType(UMLClassDiagramElement element){
		int num = 0;
		
		for( UMLClassDiagramElement elem : classDiagramElements ){
			if( elem.getClass().getName() == element.getClass().getName() ){
				num++;
			}
		}
		
		return num;
	}
	
	/**
      * Count the number of occurrence of the element typed 'typeName',
      * for example 'UMLClass'.
      * @param Enumeration, name of the UML element type.
      * @return Number of elements with the same type of 'typeName'.
	  */
	public int numberOfElementsByTypeName(UMLElementsName typeName){
		int num = 0;
		
		for( UMLClassDiagramElement elem : classDiagramElements ){
			if( elem.getClass().getName() == typeName.toString() ){
				num++;
			}
		}

		return num;
	}
	
	/**
	  * Returns a diagram element typed as 'typeName' and named as 'name'.
	  * @return UML class diagram element.
	  * @param Enumeration, name of the UML element type,
	  * 	   for example UMLClass.
	  * @param Name of the class diagram element.
	  * @throws Throws exception if there is not such element
	  * 		with this in the diagram (TheoreIsNoElementWithThisName!).
	  */
	public UMLClassDiagramElement getElementByName(UMLElementsName typeName, String name) 
		throws Exception{
			for( UMLClassDiagramElement elem : classDiagramElements ){
				if( elem.getClass().getName() == typeName.toString() && elem.getName() == name ){
					return elem;
				}
			}
		
			throw new Exception("ThereIsNoElementWithThisName");
	}
}


/*
 * "Como era dificil acreditar que ia chegar onde estou,
 * que minha vida ia mudar e mudou." 
 * 
 */
