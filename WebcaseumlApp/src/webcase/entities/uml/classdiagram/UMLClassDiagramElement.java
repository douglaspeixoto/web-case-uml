package webcase.entities.uml.classdiagram;

import webcase.entities.uml.UMLElement;

/**
 * Entity of the UMLClassDiagraElement. 
 * All entities of class diagram must extends this class.
 * 
 * @author Douglas Alves Peixoto
 */
public abstract class UMLClassDiagramElement extends UMLElement{
	
	/**
      * Default constructor.
	  */  
	public UMLClassDiagramElement() {
	}
	
}
