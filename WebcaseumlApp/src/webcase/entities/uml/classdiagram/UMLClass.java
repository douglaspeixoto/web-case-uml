package webcase.entities.uml.classdiagram;

import java.util.List;
import java.util.ArrayList;

import webcase.entities.uml.TreatAttributes;
import webcase.entities.uml.TreatOperations;
import webcase.entities.uml.UMLAttribute;
import webcase.entities.uml.UMLOperation;
import webcase.entities.uml.classdiagram.profile.UMLRelationship;

/**
 * Entity of the element UMLClass.
 * Contains its properties.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLClass extends UMLClassDiagramElement implements TreatAttributes, TreatOperations {
	private boolean isActive;
	private boolean isAbstract;
	private boolean isGeneralization;
	private boolean isSpecialization;
	private List<UMLAttribute> attributes;
	private List<UMLOperation> operations;
	private List<UMLRelationship> relationships;
	private List<UMLClass> superclasses;
	private List<UMLClass> subclasses;

	/**
      * Default constructor.
      * Sets the class name.
      * Sets the class as non-active.
	  * Sets the class as non-abstract.
	  * Sets the class as non-specialization.
	  * Sets the class as non-generalization.
	  * @param Class name. 
	  */	
	public UMLClass(String name) {
		setName( name );
		isActive = false;
		isAbstract = false;
		isGeneralization = false;
		isSpecialization = false;
		attributes = null;
		operations = null;
		relationships = null;
		superclasses = null;;
		subclasses = null;
	}

	/**
      * Returns if the class is active.
	  * @return Returns true if the class is active.
	  */
	public boolean isActive() {
		return isActive;
	}
	
	/**
      * Tells if the class is abstract.
	  * @return Returns true if the class is abstract.
	  */
	public boolean isAbstract() {
		return isAbstract;
	}

	/**
      * Tells if the class is a generalization.
	  * @return Returns true if the class is a generalization.
	  */
	public boolean isGeneralization() {
		return isGeneralization;
	}

	/**
      * Tells if the class is a specialization.
	  * @return Returns true if the class is a specialization.
	  */
	public boolean isSpecialization() {
		return isSpecialization;
	}

	/**
      * Returns the class attributes.
	  * @return Returns a list of attributes.
	  * @throws	Throws exception if the class 
	  * 		there is no attribute.
	  */
	@Override
	public List<UMLAttribute> getAttributes() throws Exception {
		if ( attributes == null || attributes.isEmpty() ){
			throw new Exception("ThereIsNoAttribute!"); 
		}
		return attributes;
	}

	/**
      * Returns the class operations.
	  * @return Returns a list of operations.
	  * @throws	Throws exception if the class 
	  * 		there is no operation.
	  */
	@Override
	public List<UMLOperation> getOperations() throws Exception {
		if ( operations == null || operations.isEmpty() ){
			throw new Exception("ThereIsNoOperation!");
		}
		return operations;
	}

	/**
      * Returns the class relationship.
	  * @return Returns a list of relationships.
	  * @throws	Throws exception if the class 
	  * 		there is no relationship.
	  */
	public List<UMLRelationship> getRelationships() throws Exception {
		if ( relationships == null || relationships.isEmpty() ){
			throw new Exception("ThereIsNoRelationship!");
		}
		return relationships;
	}

	/**
      * Returns the super-classes of this class, if there are some.
	  * @return List of classes.
	  * @throws	Throws exception if the class is not a specialization.
	  */
	public List<UMLClass> getParents() throws Exception {
		if ( superclasses == null || superclasses.isEmpty() ){
			throw new Exception("ThereIsNoSuperclass!"); 
		}
		return superclasses;		
	}

	/**
      * Returns the sub-classes of this class, if there are some.
	  * @return List of classes.
	  * @throws	Throws exception if the class is not a generalization.
	  */
	public List<UMLClass> getChildren() throws Exception {
		if ( subclasses == null || subclasses.isEmpty() ){
			throw new Exception("ThereIsNoSubclass!");
		}
		return subclasses;		 
	}

	/**
      * Sets if the class is or is not active.
      * @param True or False.
	  */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	/**
      * Sets if the class is or is not abstract.
      * @param True or False.
	  */
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
      * Sets if the class is or is not generalization.
      * @param True or False.
	  */
	public void setGeneralization(boolean generalization) {
		this.isGeneralization = generalization;
	}

	/**
      * Sets if the class is or is not specialization.
      * @param True or False.
	  */
	public void setSpecialization(boolean specialization) {
		this.isSpecialization = specialization;
	}

	/**
      * Adds an attribute to the class.
      * @param UML attribute.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the class already has the
      * 		this attribute (AttributeAlreadyAdded!).
      * @throws Throws exception if the class already has an
      * 		attribute with its name (AttributeNameAlreadyAdded!).
	  */
	@Override
	public boolean addAttribute(UMLAttribute attr) throws Exception {
		if ( attributes == null ){
			attributes = new ArrayList<UMLAttribute>();
		}
		
		// Throws exception if it's already in the class.
		if( attributes.contains( attr ) ){
			throw new Exception("AttributeAlreadyAdded!");
		}
		// Throws exception if it's already has an attribute with its name 
		for( UMLAttribute elem : attributes ){
			if( attr.getName() == elem.getName() ){
				throw new Exception("AttributeNameAlreadyAdded!");
			}
		}
		
		attributes.add( attr );
		
		return true;
	}

	/**
      * Adds an operation to the class.
      * @param UML operation.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exceptions if the class already has 
	  * 		this operation (OperationAlreadyAdded!).
  	  * @throws Throws exception if the class already has an
      * 		operation with its name (OperationNameAlreadyAdded!).
	  */
	@Override
	public boolean addOperation(UMLOperation operation) throws Exception {
		if ( operations == null ){ 
			operations = new ArrayList<UMLOperation>();
		}
		
		// Throws exception if it's already in the class.
		if( operations.contains( operation ) ){
			throw new Exception("OperationAlreadyExists!"); 
		}
		for( UMLOperation elem : operations ){
			if( operation.getName() == elem.getName() ){
				throw new Exception("OperationNameAlreadyAdded!");
			}
		}
		
		operations.add( operation );
		
		return true;
	}

	/**
      * Adds a relationship to the class.
      * @param UML relationship.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the class already has
      * 		this relationship (RelationshipAlreadyAdded!).
      * @throws Throws exception if the class already has a 
      * 		relationship with its name (RelationshipNameAlreadyAdded!).
	  */
	public boolean addRelationship(UMLRelationship relationship) throws Exception {
		if ( relationships == null ){
			relationships = new ArrayList<UMLRelationship>();
		}
		
		// Throws exception if it's already in the class.
		if( relationships.contains( relationship ) ){
			throw new Exception("RelationshipAlreadyAdded!");
		}
		for( UMLRelationship elem : relationships ){
			if( relationship.getName() == elem.getName() ){
				throw new Exception("RelationshipNameAlreadyAdded!");
			}
		}
		
		relationships.add( relationship );
		
		return true;
	}

	/**
      * Add a superclass to the class.
      * Sets the class as specialization if the 
      * superclass has been added.
      * @param UML class.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the class already has
      * 		this superclass (SuperclassAlreadyAdded!).
 	  * @throws Throws exception if the class already has a
      * 		superclass with its name (SuperclassNameAlreadyExists!).
	  */
	public boolean addSuperclass(UMLClass superclass) throws Exception {
		if ( superclasses == null ){
			superclasses = new ArrayList<UMLClass>();
		}
		
		// Throws exception if it's already in the class.
		if( superclasses.contains( superclass ) ){
			throw new Exception("SuperclassAlreadyAdded!");
		}
		for( UMLClass elem : superclasses ){
			if( superclass.getName() == elem.getName() ){
				throw new Exception("SuperclassNameAlreadyExists!");
			}
		}
		
		superclasses.add( superclass );
		
		// Sets this class as a specialization
		this.setSpecialization( true );
		
		return true;
	}

	/**
      * Add a subclass to the class.
      * Sets the class as generalization if the
      * subclass has been added.
      * @param UML class.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the class already has
      * 		this subclass (SubclassAlreadyAdded!).
      * @throws Throws exception if the class already has a
      * 		subclass with this name (SubclassNameAlreadyExists!).
	  */
	public boolean addSubclass(UMLClass subclass) throws Exception {
		if ( subclasses == null ){
			subclasses = new ArrayList<UMLClass>();
		}
		
		// Throws exception if it's already in the class.
		if( subclasses.contains( subclass ) ){
			throw new Exception("SubclassAlreadyAdded!"); 
		}
		for( UMLClass elem : subclasses ){
			if( subclass.getName() == elem.getName() ){
				throw new Exception("SubclassNameAlreadyExists!");
			}
		}
		
		subclasses.add( subclass );
		
		// Sets this class as a generalization
		this.setGeneralization( true );
		
		return true;
	}

	/**
      * Removes an attribute of the class.
      * @param Attribute to be removed.
      * @return Return true if it has been successfully removed.
      * @throws	Throws exception if the class has no attribute 
      * 		(ThereIsNotAttributes!).
      * @throws	Throws exception if the class has not the attribute
      * 		to be deleted (ThereIsNotSuchAttribute!).
	  */
	@Override
	public boolean removeAttribute(UMLAttribute attr) throws Exception {
		if ( attributes == null || attributes.isEmpty() ){
			throw new Exception("ThereIsNotAttributes!"); 
		}
		else if ( !attributes.remove( attr ) ){
			throw new Exception("ThereIsNotSuchAttribute!"); 
		}
		return true;
	}

	/**
      * Removes an operation of the class.
      * @param Operation to be deleted.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception is the class has no operation 
      * 		(ThereIsNoOperation!).
      * @throws	Throws exception if the class has not the
      * 		operation to be removed (ThereIsNotSuchOperation!).
	  */
	@Override
	public boolean removeOperation(UMLOperation op) throws Exception {
		if ( operations == null || operations.isEmpty() ){
			throw new Exception("ThereIsNoOperation!"); 
		}
		else if ( !operations.remove( op ) ){
			throw new Exception("ThereIsNotSuchOperation!");
		}
		return true;			
	}

	/**
      * Removes an relationship of the class.
      * @param Relationship to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exceptions if the class has no relationship 
      * 		(ThereIsNoRelationship!).
      * @throws	Throws exceptions if the class has not the 
      * 		relationship to be removed (ThereIsNotSuchRelationship!).
	  */
	public boolean removeRelationship(UMLRelationship rs) throws Exception {
		if ( relationships == null || relationships.isEmpty() ){
			throw new Exception("ThereIsNoRelationship!"); 
		}
		else if ( !relationships.remove( rs ) ){
			throw new Exception("ThereIsNotSuchRelationship!");
		}
		return true;
	}
	
	/**
      * Removes a superclass of the class.
      * Sets the class as non-specialization if it
      * is the remaining superclass of this class.
      * @param Class to be removed.
      * @return Return true if it has been successfully removed.
      * @throws	Throws exception if the class has no superclass
      * 		(ThereIsNoSuperclass!).
      * @throws	Throws exception if the class already has not the
      * 		superclass to be deleted (ThereIsNotSuchSuperclass!).
	  */	
	public boolean removeSuperclass(UMLClass sc) throws Exception {
		if ( superclasses == null || superclasses.isEmpty() ){
			throw new Exception("ThereIsNoSuperclasse!");
		}
		else if ( !superclasses.remove( sc ) ){
			throw new Exception("ThereIsNotSuchSuperclass!"); 
		}
		
		// Sets the class as non-specialization if it has no more super-classes.
		if( superclasses.isEmpty() ){
			this.setSpecialization( false );
		}
		
		return true;
	}

	/**
      * Removes a superclass of the class.
      * Sets the class as non-generalization if it
      * is the remaining superclass of this class.
      * @param Class to be deleted.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the class has no subclass 
      * 		(ThereIsNoSubclasse!).
      * @throws	Throws exception if the class already has not the
      * 		subclass to be deleted (ThereIsNotSuchSubclass!).
	  */
	public boolean removeSubclass(UMLClass sb) throws Exception {
		if ( subclasses == null || subclasses.isEmpty() ){
			throw new Exception("ThereIsNoSubclasse!"); 
		}
		else if ( !subclasses.remove( sb ) ){
			throw new Exception("ThereIsNotSuchSubclass!"); 
		}
		
		// Sets the class as non-generalization if it has no more subclasses.
		if( subclasses.isEmpty() ){
			this.setSpecialization( false );
		}
		
		return true;
	}

	/**
      * Returns the number of attribute from the class.
      * @return Number of attributes from the class.
      */
	@Override
	public int numberOfAttributes(){
		return attributes.size();
	}
	
	/**
      * Returns the number of operations from the class.
      * @return Number of operations from the class.
      */
	@Override
	public int numberOfOperations(){
		return operations.size();
	}
	
	/**
	  * Returns the attribute named as 'name'.
	  * @return UML attribute.
	  * @throws Throws exception if the class has not an attribute
	  * 		named as 'name' (ThereIsNotSuchAttribute!).
	  */
	@Override
	public UMLAttribute getAttributeByName(String name) throws Exception{
		for( UMLAttribute elem : attributes ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchAttribute!");
	}
	
	/**
	  * Returns the operation named as 'name'.
	  * @return UML operation.
	  * @throws Throws exception if the class has not an operation
	  * 		named as 'name' (ThereIsNotSuchOperation!).
	  */
	@Override
	public UMLOperation getOperationByName(String name) throws Exception{
		for( UMLOperation elem : operations ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchOperation!");
	}

	/**
	  * Returns the superclass named as 'name'.
	  * @return UML class.
	  * @throws Throws exception if the class has not a superclass
	  * 		named as 'name' (ThereIsNotSuchSuperclass!).
	  */
	public UMLClass getSuperclassByName(String name) throws Exception{
		for( UMLClass elem : superclasses ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchSuperclass!");
	}

	/**
	  * Returns the subclass named as 'name'.
	  * @return UML class.
	  * @throws Throws exception if the class has not a superclass
	  * 		named as 'name' (ThereIsNotSuchSubclass!).
	  */
	public UMLClass getSubclassByName(String name) throws Exception{
		for( UMLClass elem : subclasses ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchSubclass!");
	}
}


/*
 * "Eu sou cachorro louco que anda solto no mundo,
 * sem tempo pra ser nada al�m de um vagabundo."
 */