package webcase.entities.uml.classdiagram.profile;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.Window;

import webcase.entities.uml.TreatAttributes;
import webcase.entities.uml.TreatOperations;
import webcase.entities.uml.UMLAttribute;
import webcase.entities.uml.UMLOperation;
import webcase.entities.uml.classdiagram.UMLClass;
import webcase.entities.uml.standard.UMLStandardProfile;

/**
 * Entity of the element UMLRelationship.
 * Contains its properties.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLStereotype extends UMLProfileElement implements TreatAttributes, TreatOperations {
	private boolean isAbstract;
	private UMLMetaclass extendedMetaclass;
	private List<UMLAttribute> taggedValues;
	private List<UMLOperation> operations;
	private List<UMLStereotype> superclasses;
	private List<UMLStereotype> subclasses;

	/**
      * Default constructor.
      * Sets the stereotype name.
      * Sets the enumeration standard stereotype.
      * Sets the stereotype as non-abstract.
      * @param Stereotype name. 
	  */
	public UMLStereotype(String name) {
		try {
			// Apply the stereotype 'Stereotype' to this element
			applyStereotype( UMLStandardProfile.getStereotypeByName( "Stereotype" ) );
		} catch (Exception e) {
			Window.alert("Stereotype Constructor Alert!\n" +
						"Cannot apply stererotype to 'UMLStereotype' element.\n" +
						"Element name: '" + name + "'.");
			e.printStackTrace();
		} finally {
			setName( name );
			isAbstract = false;
			taggedValues = null; 
			operations = null; 
			superclasses = null; 
			subclasses = null;
		} 
	}

	/**
      * Returns if the stereotype is abstract.
	  * @return Returns true if the stereotype is abstract.
	  */
	public boolean isAbstract() {
		return isAbstract;
	}
	
	/**
      * Returns the metaclass of the stereotype (which UML element
      * this stereotype extends)
      * @return Metaclass (Enumeration).
	  */
	public UMLMetaclass getExtendedMetaclass() {
		return extendedMetaclass;
	}

	/**
      * Returns a list of stereotype tagged values.
	  * @return List of tagged values (UML attributes) 
	  * 		from the stereotype.
	  * @throws	Throws exception if there are not tagged values
	  * 		in the stereotype (ThereIsNoTaggedValue!).
	  */
	@Override
	public List<UMLAttribute> getAttributes() throws Exception {
		if ( taggedValues == null || taggedValues.isEmpty() ){
			throw new Exception("ThereIsNoTaggedValue!");
		}
		return taggedValues;
	}

	/**
      * Returns a list of stereotype operations.
	  * @return List of operations from the stereotype.
	  * @throws	Throws exception if there are not operations
	  * 		in the stereotype (ThereIsNoOperation!).
	  */
	@Override
	public List<UMLOperation> getOperations() throws Exception {
		if ( operations == null || operations.isEmpty() ){
			throw new Exception("ThereIsNoOperation!");
		}
		return operations;
	}
	
	/**
      * Returns a list of superclasses of the stereotype.
	  * @return List of classes.
	  * @throws	Throws exception if the stereotype have no 
	  * 		superclasses (ThereIsNoSuperclass!).
	  */
	public List<UMLStereotype> getParents() throws Exception {
		if ( superclasses == null || superclasses.isEmpty() ){
			throw new Exception("ThereIsNoSuperclass!");
		}
		return superclasses;
	}
	
	/**
      * Returns a list of subclasses of the stereotype.
	  * @return List of classes.
	  * @throws	Throws exception if the stereotype have no 
	  * 		subclasses (ThereIsNoSubclass!).
	  */
	public List<UMLStereotype> getChildren() throws Exception {
		if ( subclasses == null || subclasses.isEmpty() ){
			throw new Exception("ThereIsNoSubclass!");
		}
		return subclasses;
	}
	
	/**
      * Sets if the stereotype is abstract.
      * @param True or False.
	  */
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
      * Sets the metaclass which this stereotype extends.
      * @param Metaclass name (Enumeration).
	  */
	public void setExtendedMetaclass(UMLMetaclass metaclass) {
		this.extendedMetaclass = metaclass;
	}

	/**
      * Adds a tagged value (UML attribute) to the stereotype.
      * @param Tagged value (UMLAttribute) to be added.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the stereotype already has the 
      * 		tagged value to be added (TaggedValueAlreadyExists!).
      * @throws Throws Exception if the stereotype already has a 
      * 		tagged value with this name (TaggedValueNameAlreadyExists!).
	  */
	@Override
	public boolean addAttribute(UMLAttribute taggedValue) throws Exception {
		if (taggedValues == null){
			taggedValues = new ArrayList<UMLAttribute>();
		}
		
		// Throws exception if it's already in the stereotype.
		if( taggedValues.contains( taggedValue ) ){
			throw new Exception("TaggedValueAlreadyExists!"); 
		}
		for( UMLAttribute elem : taggedValues ){
			if( taggedValue.getName() == elem.getName() ){
				throw new Exception("TaggedValueNameAlreadyExists!");
			}
		}
		
		taggedValues.add( taggedValue );
		
		return true;
	}

	/**
      * Adds an operation to the stereotype.
      * @param Operation to be added.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the stereotype already has the 
      * 		operation to be added (TaggedValueAlreadyExists!).
      * @throws Throws Exception if the stereotype already has a 
      * 		operation with this name (TaggedValueNameAlreadyExists!).
	  */
	@Override
	public boolean addOperation(UMLOperation operation) throws Exception {
		if (operations == null ){
			operations = new ArrayList<UMLOperation>();
		}
		
		// Throws exception if it's already in the stereotype.
		if( operations.contains( operation ) ){
			throw new Exception("OperationAlreadyExists!");
		}
		for( UMLOperation elem : operations ){
			if( operation.getName() == elem.getName() ){
				throw new Exception("OperationNameAlreadyExists!");
			}
		}
		
		operations.add( operation );
		
		return true;
	}

	/**
      * Adds a superclass to the stereotype.
      * @param Class.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the stereotype already has the 
      * 		superclass to be added (SuperclassAlreadyExists!).
      * @throws Throws exception if the stereotype already has a superclass
      * 		with this name (SuperclassNameAlreadyExists!).
	  */
	public boolean addSuperclass(UMLStereotype superclass) throws Exception {
		if (superclasses == null){
			superclasses = new ArrayList<UMLStereotype>();
		}
		
		// Throws exception if it's already in the stereotype.
		if( superclasses.contains( superclass ) ){
			throw new Exception("SuperclassAlreadyExists!");
		}
		for( UMLStereotype elem : superclasses ){
			if( superclass.getName() == elem.getName() ){
				throw new Exception("SuperclassNameAlreadyExists!");
			}
		}
		
		superclasses.add( superclass );
		
		return true;
	}

	/**
      * Adds a subclass to the stereotype.
      * @param Class.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the stereotype already has the 
      * 		subclass to be added (SubclassAlreadyExists!).
      * @throws Throws exception if the stereotype already has a
      * 		subclass whit this name (SubclassNameAlreadyExists!).
	  */
	public boolean addSubclass(UMLStereotype subclass) throws Exception {
		if (subclasses == null){
			subclasses = new ArrayList<UMLStereotype>();
		}
		
		// Throws exception if it's already in the stereotype.
		if( subclasses.contains( subclass ) ){
			throw new Exception("SubclassAlreadyExists!");
		}
		for( UMLStereotype elem : subclasses ){
			if( subclass.getName() == elem.getName() ){
				throw new Exception("SubclassNameAlreadyExists!");
			}
		}
		
		subclasses.add( subclass );
		
		return true;
	}
	
	/**
      * Removes a tagged value (UML attribute) from the stereotype.
      * @param Tagged Value (UMLAttribute) to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the stereotype has no tagged value 
      * 		(ThereIsNoTaggedValue!).
      * @throws	Throws exception if the stereotype has not the tagged
      * 		value to be removed (ThereIsNotSuchTaggedValue!).
	  */
	@Override
	public boolean removeAttribute(UMLAttribute tValue) throws Exception {
		if ( taggedValues == null || taggedValues.isEmpty() ){
			throw new Exception("ThereIsNoTaggedValue!");
		}
		else if ( !taggedValues.remove( tValue ) ){
			throw new Exception("ThereIsNotSuchTaggedValue!");
		}
		return true;
	}

	/**
      * Removes an operation from the stereotype.
      * @param Operation to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the stereotype has no operation 
      * 		(ThereIsNoTaggedValue!).
      * @throws	Throws exception if the stereotype has not the tagged
      * 		value to be removed (ThereIsNotSuchTaggedValue!).
	  */
	@Override
	public boolean removeOperation(UMLOperation op) throws Exception {
		if ( operations == null || operations.isEmpty() ){
			throw new Exception("ThereIsNotOperations!");
		}
		else if ( !operations.remove( op ) ){
			throw new Exception("ThereIsNotSuchOperation!");
		}		
		return true;
	}

	/**
      * Removes a superclass of the stereotype.
      * @param Superclass to be deleted.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the stereotype have no 
      * 		superclass (ThereIsNotSuperclasses!).
      * @throws	Throws exception if the stereotype has not the
      * 		superclass to be removed (ThereIsNotSuchSuperclass!).
	  */
	public boolean removeSuperclass(UMLClass sc) throws Exception {
		if ( superclasses == null || superclasses.isEmpty() ){
			throw new Exception("ThereIsNotSuperclasses!");
		}
		else if ( !superclasses.remove( sc ) ){
			throw new Exception("ThereIsNotSuchSuperclass!");
		}
		return true;
	}

	/**
      * Removes a subclass of the stereotype.
      * @param Subclass to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the stereotype has no 
      * 		subclass (ThereIsNoSubclass!).
      * @throws	Throws exception if  the stereotype has not the
      * 		subclass to be removed (ThereIsNotSuchSubclass!).
	  */
	public boolean removeSubclass(UMLClass sb) throws Exception {
		if ( subclasses == null || subclasses.isEmpty() ){
			throw new Exception("ThereIsNoSubclass!");
		}
		else if ( !subclasses.remove( sb ) ){
			throw new Exception("ThereIsNotSuchSubclass!");
		}
		return true;
	}
	
	/**
      * Returns the number of tagged values (UML attributes)
      * of the stereotype.
      * @return Number of tagged values of the stereotype.
      */
	@Override
	public int numberOfAttributes(){
		return taggedValues.size();
	}
	
	/**
      * Returns the number of operations of the stereotype.
      * @return Number of operations of the stereotype.
      */
	@Override
	public int numberOfOperations() {
		return operations.size();
	}

	/**
	  * Returns the tagged value named as 'name'.
	  * @return Tagged value (UML attribute).
	  * @throws Throws exception if the stereotype has not an tagged
	  * 		value named as 'name' (ThereIsNotSuchTaggedValue!).
	  */
	@Override
	public UMLAttribute getAttributeByName(String name) throws Exception{
		for( UMLAttribute elem : taggedValues ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchTaggedValue!");
	}

	/**
	  * Returns the operation named as 'name'.
	  * @return UML operation.
	  * @throws Throws exception if the stereotype has not an operation
	  * 		named as 'name' (ThereIsNotSuchOperation!).
	  */
	@Override
	public UMLOperation getOperationByName(String name) throws Exception {
		for( UMLOperation elem : operations ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchOperation!");
	}
	
	/**
	  * Returns the superclass named as 'name'.
	  * @return UML stereotype.
	  * @throws Throws exception if the stereotype has not a superclass
	  * 		named as 'name' (ThereIsNotSuchSuperclass!).
	  */
	public UMLStereotype getSuperclassByName(String name) throws Exception{
		for( UMLStereotype elem : superclasses ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchSuperclass!");
	}
	
	/**
	  * Returns the subclass named as 'name'.
	  * @return UML stereotype.
	  * @throws Throws exception if the stereotype has not a superclass
	  * 		named as 'name' (ThereIsNotSuchSubclass!).
	  */
	public UMLStereotype getSubclassByName(String name) throws Exception{
		for( UMLStereotype elem : subclasses ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchSubclass!");
	}

}