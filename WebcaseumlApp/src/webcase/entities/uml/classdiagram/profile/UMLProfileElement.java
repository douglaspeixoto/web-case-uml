package webcase.entities.uml.classdiagram.profile;

import webcase.entities.uml.classdiagram.UMLClassDiagramElement;

/**
 * Entity of the UMLProfileElement. 
 * All entities of profile must extends this class.
 * 
 * @author Douglas Alves Peixoto
 */
public abstract class UMLProfileElement extends UMLClassDiagramElement{
	
	/**
      * Default constructor.
	  */  
	public UMLProfileElement() {
	}
	
}
