package webcase.entities.uml.classdiagram.profile;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity of the element UMLProfile.
 * Contains its properties and informations,
 * name of the profile and its elements.
 * 
 * @author Douglas Alves Peixoto
 */
// Hereafter should be created a method which validates
// the stereotypes added in a element. For example, if two
// incompatible stereotypes are being applied in a element,
// according to the OCL constraints, but implement this in java.

// Then, the created profiles must call this method every time
// that a stereotype is applied to an element.

public class UMLProfile {
	private String profileName;
	private List<UMLProfileElement> profileElements;
	
	/**
      * Default constructor.
      * Sets the profile name.
      * @param Profile name.
	  */
	public UMLProfile(String name) {
		setName( name );
		profileElements = null;
	}

	/**
      * Returns the profile name.
	  * @return Profile name.
	  */
	public String getName() {
		return profileName;
	}

	/**
	  * Returns a list with all elements of the profile.
	  * @return List of profile elements.
	  * @throws	Throws exception if there is no element
	  * 		in the profile (ThereIsNoElement!).
	  */
	public List<UMLProfileElement> getProfileElements() throws Exception {
		if ( profileElements == null || profileElements.isEmpty() ){
			throw new Exception("ThereIsNoElement!");
		}
		
		return profileElements;
	}
	
	/**
      * Returns list of the profile stereotypes.
      * @return Profile stereotypes.
      * @throws Throws exception if there is not stereotypes 
      * 		in the profile (ThereIsNoStereotype!).
	  */
	public List<UMLStereotype> getStereotypes() throws Exception{
		List<UMLStereotype> list = new ArrayList<UMLStereotype>();
		
		if( profileElements == null ){
			throw new Exception("ThereIsNoStereotype!");
		}
		for( UMLProfileElement elem : profileElements ){
			if( elem.getClass().getName() == "UMLSterotype" ){
				list.add( (UMLStereotype) elem );
			}
		}
		
		return list;
	}	
	
	/**
      * Sets the profile name.
      * @param Profile name.
      * @return Returns false if the profile name is not valid,
      * 		(can not start with number).
      */	
	public boolean setName(String name) {
	    // name must not start with number.
	    if( name.substring(0,1).matches("[0-9]") ){
	    	return false;
	    }
	    
		this.profileName = name;
		
		return true;
	}	

	/**
      * Adds a list o profile elements.
      * @param List of profile elements.
      */  
	public void setProfileElements(List<UMLProfileElement> profileElements) {
		this.profileElements = profileElements;
	}

	/**
      * Adds an element to the profile.
      * @param Profile UML element.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the profile already has the 
      * 		element to be added (ElementAlreadyExists!).
      * @throws Throws exception if the profile already has an element
      * 		of the same type and same name (ElementNameAlreadyExists!).
	  */	
	public boolean addProfileElement(UMLProfileElement element) throws Exception {
		if ( profileElements == null ){
			profileElements = new ArrayList<UMLProfileElement>();
		}
		
		// Throws exception if it's already in the profile.
		if( profileElements.contains( element ) ){
			throw new Exception("ElementAlreadyExists!");
		}
		// Throws exception if exists an object of the same type (Class name)
		// and same name. 
		for ( UMLProfileElement elem : profileElements ){
			if ( element.getClass().getName() == elem.getClass().getName() && 
				 element.getName() == elem.getName() ){
				 throw new Exception("ElementNameAlreadyExists!");
			}
		}
		
		profileElements.add( element );
		
		return true;
	}
	
	/**
      * Removes a profile element.
      * @param Profile element to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws Throws exception if the profile has no element 
      * 		(ThereIsNotElements!).
      * @throws Throws exceptions if the profile has not the 
      * 		element to be removed (ThereIsNotSuchElement!).
	  */
	public boolean removeProfileElement(UMLProfileElement element) throws Exception {
		if ( profileElements == null || profileElements.isEmpty() ){
			throw new Exception("ThereIsNotElements!");
		}
		else if ( !profileElements.remove( element ) ){
			throw new Exception("ThereIsNotSuchElement!");
		}
		return true;
	}

	/**
	 * Return a profile stereotype by its name.
	 * @param Stereotype name.
	 * @return Stereotype.
	 * @throws Throws exception if the stereotype does not 
     *		   exist in the profile (ThereIsNotSuchStereotype!).
	 */
	public UMLStereotype getStereotypeByName(String strName) throws Exception{
		for( UMLProfileElement elem : profileElements ){
			if( elem.getName() == strName &&
				elem.getClass().getName() == UMLMetaclass.UMLStereotype.name() ){
				return (UMLStereotype)elem;
			}
		}
		throw new Exception("ThereIsNotSuchStereotype");
	}
	
	// Objects name change must be done through profile.
	/**
      * Change the profile element name.
      * Objects change of name must be done through profile
      * to avoid duplicate.
      * @param Profile element to be changed.
      * @param New element name.
      * @return Returns true if it has been successfully changed.
      * @throws	Throws exception if the profile already has an element
      * 		of the same type and name (NameAlreadyExists!).
	  */	
	public boolean changeElementName(UMLProfileElement element, String newName) throws Exception{
		if( hasDuplicateName(element, newName) ){
			throw new Exception("NameAlreadyExists!");
		}
		
		element.setName( newName );
		
		return true;
	}
	
	/**
      * Verify if there are some element typed with the same type of 'element'
      * with same name (if the name is doubled).
      * @param Profile element to be searched.
      * @param Element name to be searched.
      * @return Returns true if there is some element with same type of 
      * 		'element' named as 'name'. Returns false otherwise.
	  */
	public boolean hasDuplicateName(UMLProfileElement element, String name){		
		for( UMLProfileElement elem : profileElements ){
			if( elem.getClass().getName() == element.getClass().getName() &&
				elem.getName() == name && !elem.equals( element ) ){
				return true;
			}
		}
		return false;
	}
	
	/**
	  * Count the number of elements with same type of 'metaclass' 
      * in the profile. 'metaclass' can be any element (enumeration)
      * which can be applied stereotypes, for example, UMLClass.
      * @param Profile element which will count the number
      *		   of elements in the profile (Enumeration).
      * @return Number of elements with the type of 'metaclass'.
	  */
	public int numberOfElementsByType(UMLMetaclass metaclass){
		int num = 0;
		
		if( profileElements == null || profileElements.isEmpty() ){
			return 0;
		}
		
		for( UMLProfileElement elem : profileElements ){
			if( elem.getClass().getName() == metaclass.name() ){
				num++;
			}
		}
		
		return num;
	}

	/**
      * Returns the number of stereotypes in the profile.
      * It returns the same result of 
      * numberOfElementsByType( UMLMetaclass.UMLStereotype )
      * @return Number of stereotypes in the profile.
      */
	public int numberOfStereotypes() {
		return numberOfElementsByType( UMLMetaclass.UMLStereotype );
	}
	
}
