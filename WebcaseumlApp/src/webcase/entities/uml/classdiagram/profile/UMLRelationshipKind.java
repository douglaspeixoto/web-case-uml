package webcase.entities.uml.classdiagram.profile;

/**
 * @author Douglas Alves Peixoto
 */
public enum UMLRelationshipKind {
	ASSOCIATION, 
	COMPOSITION, 
	AGGREGATION;
}