package webcase.entities.uml.classdiagram.profile;

import webcase.entities.uml.classdiagram.UMLClass;

/**
 * Entity of the element UMLRelationship.
 * Contains its properties.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLRelationship extends UMLProfileElement {
	private boolean isDirectional;
	private UMLClass classBegin;
	private UMLClass classEnd;
	private String beginRoleName;
	private String endRoleName;
	private UMLMultiplicity multiplicityBegin;
	private UMLMultiplicity multiplicityEnd;
	private UMLRelationshipKind relationshipKind;
	private UMLVisibility visibilityBegin;
	private UMLVisibility visibilityEnd;

	/**
      * Default constructor.
      * Sets the relationship name.
      * Sets directional as false.
      * Sets initial and final multiplicity as ZERO_TO_ONE.
      * Sets relationship kind as ASSOCIATION.
      * Sets initial and final visibility as PUBLIC.
      * Sets the relationship begin name as 'begin'.
      * Sets the relationship end name as 'end'.
      * @param Relationship name. 
	  */
	public UMLRelationship(String name) {
		super();
		setName( name );
		beginRoleName = new String("begin");
		endRoleName = new String("end");
		isDirectional = false;
		classBegin = null;
		classEnd = null;
		multiplicityBegin = UMLMultiplicity.ZERO_TO_ONE;
		multiplicityEnd = UMLMultiplicity.ZERO_TO_ONE;
		relationshipKind = UMLRelationshipKind.ASSOCIATION;
		visibilityBegin = UMLVisibility.PUBLIC;
		visibilityBegin = UMLVisibility.PUBLIC;
	}

	/**
      * Returns if the relationship is directional.
	  * @return True if the relationship is directional.
	  */
	public boolean isDirectional() {
		return isDirectional;
	}
	
	/**
      * Returns the class of the beginning of the relationship.
	  * @return Class.
	  */
	public UMLClass getClassBegin() {
		return classBegin;
	}

	/**
      * Returns the class of the end of the relationship.
	  * @return Class.
	  */
	public UMLClass getClassEnd() {
		return classEnd;
	}

	/**
      * Return the name of the beginning of the relationship.
	  * @return Relationship begin name.
	  */
	public String getBeginRoleName() {
		return beginRoleName;
	}

	/**
      * Return the name of the end of the relationship.
	  * @return Relationship end name.
	  */
	public String getEndRoleName() {
		return endRoleName;
	}
	
	/**
      * Returns if the relationship.
	  * @return Returns true if the relationship is a composition.
	  */
	public boolean isComposition() {
		if ( relationshipKind == UMLRelationshipKind.COMPOSITION ){
			return true;
		}
		return false;
	}

	/**
      * Returns if the relationship is an aggregation.
	  * @return Returns true if the relationship is an aggregation.
	  */
	public boolean isAggregation() {
		if ( relationshipKind == UMLRelationshipKind.AGGREGATION ){
			return true;
		}
		return false;
	}

	/**
      * Returns the lower bound of the beginning of the relationship.
	  * @return 0, 1 or -1 (to many).
	  */
	public int getBeginLowerBound() {
		if ( multiplicityBegin == UMLMultiplicity.ZERO_TO_ONE || 
			 multiplicityBegin == UMLMultiplicity.ZERO_TO_MANY ){
				return 0;
		}
		else if ( multiplicityBegin == UMLMultiplicity.ONE_TO_ONE || 
				  multiplicityBegin == UMLMultiplicity.ONE_TO_MANY ) {
				return 1;
		}
		else {
			return -1;
		}
	}

	/**
      * Returns the upper bound of the beginning of the relationship.
	  * @return 0, 1 or -1 (to many).
	  */
	public int getBeginUpperBound() {
		if ( multiplicityBegin == UMLMultiplicity.ZERO_TO_ONE || 
			 multiplicityBegin == UMLMultiplicity.ONE_TO_ONE ){
				return 1;
		}
		else {
				return -1;
		}
	}

	/**
      * Returns the lower bound of the end of the relationship.
	  * @return 0, 1 or -1 (to many).
	  */
	public int getEndLowerBound() {
		if ( multiplicityEnd == UMLMultiplicity.ZERO_TO_ONE || 
			 multiplicityEnd == UMLMultiplicity.ZERO_TO_MANY ){
				return 0;
		}
		else if ( multiplicityEnd == UMLMultiplicity.ONE_TO_ONE || 
				  multiplicityEnd == UMLMultiplicity.ONE_TO_MANY ){
				return 1;
		}
		else{
				return -1;
		}
	}

	/**
      * Returns the upper bound of the end of the relationship.
	  * @return 0, 1 or -1 (to many).
	  */
	public int getEndUpperBound() {
		if ( multiplicityEnd == UMLMultiplicity.ZERO_TO_ONE || 
			 multiplicityEnd == UMLMultiplicity.ONE_TO_ONE ) {
			return 1;
		}
		else {
			return -1;
		}
	}

	/**
      * Returns the visibility of the beginning of the relationship.
	  * @return Visibility (PUBLIC, PRIVATE or PROTECTED).
	  */
	public UMLVisibility getVisibilityBegin() {
		return visibilityBegin;
	}

	/**
      * Returns the visibility of the end of the relationship.
	  * @return Visibility (PUBLIC, PRIVATE or PROTECTED).
	  */
	public UMLVisibility getVisibilityEnd() {
		return visibilityEnd;
	}

	/**
      * Sets if the relationship is directional.
	  * @param True or False.
	  */
	public void setDirectional(boolean directional) {
		this.isDirectional = directional;
	}
	
	/**
      * Sets the class of the beginning of the relationship.
      * Sets the name of the beginning of the relationship as
      * the class name in lower case.
	  * @param Class.
	  */
	public void setClassBegin(UMLClass classBegin) {
		this.classBegin = classBegin;
		beginRoleName = classBegin.getName().toLowerCase();
	}

	/**
      * Sets the class of the end of the relationship.
      * Sets the name of the end of the relationship as
      * the class name in lower case.
	  * @param Class.
	  */
	public void setClassEnd(UMLClass classEnd) {
		this.classEnd = classEnd;
		endRoleName = classEnd.getName().toLowerCase();
	}

	/**
      * Sets the name of the beginning of the relationship.
	  * @param Name of the beginning of the relationship.
	  */
	public void setBeginRoleName(String beginName) {
		this.beginRoleName = beginName;
	}
	
	/**
      * Sets the name of the end of the relationship.
	  * @param Name of the end of the relationship.
	  */
	public void setEndRoleName(String endName) {
		this.endRoleName = endName;
	}
	
	/**
      * Sets the multiplicity of the beginning of the relationship.
	  * @param Enumeração.
	  */
	public void setMultiplicityBegin(UMLMultiplicity multiplicityBegin) {
		this.multiplicityBegin = multiplicityBegin;
	}

	/**
      * Sets the multiplicity of the end of the relationship.
	  * @param Multiplicity.
	  */
	public void setMultiplicityEnd(UMLMultiplicity multiplicityEnd) {
		this.multiplicityEnd = multiplicityEnd;
	}

	/**
      * Sets the kind of the relationship.
	  * @param Relationship kind (ASSOCIATION, COMPOSITION or AGGREGATION).
	  */
	public void setRelationshipKind(UMLRelationshipKind relationshipKind) {
		this.relationshipKind = relationshipKind;
	}

	/**
      * Sets the visibility of the beginning of the relationship.
	  * @param Visibility (PUBLIC, PRIVATE or PROTECTED).
	  */
	public void setVisibilityBegin(UMLVisibility visibilityBegin) {
		this.visibilityBegin = visibilityBegin;
	}
	
	/**
      * Sets the visibility of the end of the relationship.
	  * @param Visibility (PUBLIC, PRIVATE or PROTECTED).
	  */
	public void setVisibilityEnd(UMLVisibility visibilityEnd) {
		this.visibilityEnd = visibilityEnd;
	}
	
}