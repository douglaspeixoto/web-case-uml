package webcase.entities.uml.classdiagram.profile;

/**
 * Multiplicity of the elements (Relationships, Attributes)
 * 
 * @author Douglas Alves Peixoto
 */
public enum UMLMultiplicity {
	ZERO_TO_ONE,   // 0..1
	ZERO_TO_MANY,  // 0..*
	ONE_TO_ONE,    // 1
	ONE_TO_MANY,   // 1..*
	MANY_TO_MANY;  // *
}