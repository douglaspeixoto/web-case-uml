package webcase.entities.uml.classdiagram.profile;

import java.util.ArrayList;
import java.util.List;

import webcase.entities.uml.classdiagram.UMLClassDiagramElement;

/**
 * Entity of the element UMLPackage.
 * Contains its properties.
 * 
 * Do not need to add UMLOperation and UMLAttribute, this
 * elements are related with the elements which they belong.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLPackage extends UMLProfileElement {
	private List<UMLClassDiagramElement> elements;
	
	/**
      * Default constructor.
      * Sets the package name.
      * @param Package name. 
	  */
	public UMLPackage(String name) {
		setName( name );
		elements = null;
	}

	/**
      * Returns a list of the package elements.
	  * @return List of UML elements associated to this package.
	  * @throws	Throws exception if there is no elements
	  * 		in the package (ThereIsNoElement!).
	  */
	public List<UMLClassDiagramElement> getElements() throws Exception {
		if ( elements == null || elements.isEmpty() ){
			throw new Exception("ThereIsNoElement!");
		}
		return elements;
	}

	/**
      * Adds an element to the package.
 	  *	Do not need to add UMLOperation and UMLAttribute, this
 	  * elements are related with the elements which they belong.
      * @param UML element.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the package already has the
      * 		element to be added (ElementNameAlreadyExists!).
      * @throws Throws exception if the package already has an element 
      * 		with same name and same type (ElementNameAlreadyExists!).
	  */
	public boolean addElement(UMLClassDiagramElement element) throws Exception {
		if ( elements == null ){
			elements = new ArrayList<UMLClassDiagramElement>();
		}
	
		// Throws exception if it's already in the package.
		if ( elements.contains( element ) ){
			throw new Exception("ElementAlreadyAdded!");
		}
		// Throws exception if exists an object of the same type (Class name)
		// with the same name.
		for ( UMLClassDiagramElement elem : elements ){
			if( element.getClass().getName() == elem.getClass().getName() && 
				element.getName() == elem.getName() ){
				throw new Exception("ElementNameAlreadyExists!");	
			}
		}
		
		elements.add( element );
		
		return true;
	}

	/**
      * Removes a package element.
      * @param Element to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the package has no element 
      * 		(ThereIsNoElement!).
      * @throws	Throws exception if the package has not the element
      * 		to be removed (ThereIsNotSuchElement!).
	  */
	public boolean removeElement(UMLClassDiagramElement elem) throws Exception {
		if ( elements == null || elements.isEmpty() ){
			throw new Exception("ThereIsNoElement!");
		}
		else if ( !elements.remove( elem ) ){
			throw new Exception("ThereIsNotSuchElement!");
		}
		return true;
	}
}