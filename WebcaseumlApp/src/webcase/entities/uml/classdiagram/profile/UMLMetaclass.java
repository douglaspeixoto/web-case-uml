package webcase.entities.uml.classdiagram.profile;

import java.util.ArrayList;
import java.util.List;

/**
 * UML Elements which can be applied stereotypes.
 * 
 * If a new UML element is added in the program, and
 * stereotypes can be applied to it, its name 
 * must be added to this enumeration exactly
 * how it is declared in the class name.
 * 
 * @author Douglas Alves Peixoto
 */
public enum UMLMetaclass {
	UMLClass,
	UMLRelationship,
	UMLAttribute,
	UMLOperation,
	UMLPackage,
	UMLStereotype,
	UMLDataType,
	UMLEnumeration;
	
	/**
	  * Returns a enumeration list with the name of the
	  * UML elements which can be applied stereotypes. 
	  * @return List of metaclasses.
	  */
	// If a new metaclass is added in this enumeration
	// it must be added in this function as well.
	public static final List<UMLMetaclass> getMetaclasses(){
		ArrayList<UMLMetaclass> meta = new ArrayList<UMLMetaclass>();
		
		meta.add( UMLMetaclass.UMLClass );
		meta.add( UMLMetaclass.UMLAttribute );
		meta.add( UMLMetaclass.UMLOperation );
		meta.add( UMLMetaclass.UMLPackage );
		meta.add( UMLMetaclass.UMLRelationship );
		meta.add( UMLMetaclass.UMLStereotype );
		meta.add( UMLMetaclass.UMLDataType );
		meta.add( UMLMetaclass.UMLEnumeration );
		
		return meta;
	}
}
