package webcase.entities.uml.classdiagram.profile;

/**
 * @author Douglas Alves Peixoto
 */
public enum UMLVisibility {
	PUBLIC, 
	PRIVATE, 
	//PACKAGE,
	PROTECTED;
}