package webcase.entities.uml.classdiagram.profile;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.Window;

import webcase.entities.uml.standard.UMLStandardProfile;

/**
  * Entity of the element UMLEnumeration.
  * Contains its properties.
  * 
  * @author Douglas Alves Peixoto
  */
public class UMLEnumeration extends UMLProfileElement {
	private boolean isAbstract;
	private List<String> literals;

	/**
	  * Default constructor.
	  * Sets the enumeration name.
	  * Sets the enumeration standard stereotype.
	  * Sets the enumeration as not abstract.
	  * @param Enumeration name.
	  */
	public UMLEnumeration(String name) {
		try {
			// Apply the stereotype 'Enumeration' to this element
			applyStereotype( UMLStandardProfile.getStereotypeByName( "Enumeration" ) );
		} catch (Exception e) {
			Window.alert("Enumeration Constructor Alert!\n" +
						"Coudn't apply stererotype to 'UMLEnumeration' element.\n" +
						"Element name: '" + name + "'");
			e.printStackTrace();
		} finally {
			setName( name );
			isAbstract = false;
			literals = null;
		}	
	}
	
	/**
      * Return if the enumeration is abstract.
	  * @return Return true if the enumeration is abstract
	  * 		or false otherwise.
	  */
	public boolean isAbstract() {
		return isAbstract;
	}

	/**
      * Returns list of literals from this enumeration.
	  * @return List of literals.
	  * @throws	Throws exception if there is no
	  * 		literals in the enumeration.
	  */
	public List<String> getLiterals() throws Exception {
		if ( literals == null || literals.isEmpty() ){
			throw new Exception("ThereIsNoLiterals!");
		}
		return literals;
	}
	
	/**
      * Sets if the enumeration is abstract.
      * @param True or False.
	  */
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}
	
	/**
      * Add a literal in the enumeration.
      * @param Literal.
      * @return Returns true if it was successfully added.
      *         Returns false if the literal name is not valid.
	  * @throws Throws exception if it's already in the enumeration.
      * 		(LiteralAlreadyExists!).
      * @throws Throws exception if it already has a literal
      * 		with this name (LiteralNameAlreadyExists!).
	  */
	public boolean addLiteral(String literal) throws Exception {
		if ( literals == null ){
			literals = new ArrayList<String>();
		}
		
		// literal name must be between 1 and 20 chars that are numbers, 
		// letters or underline.
	    if ( !literal.matches("^[0-9a-zA-Z\\_]{1,20}$") ) {
	    	return false; 
	    }
	    // literal name must not start with number.
	    if( literal.substring(0,1).matches("[0-9]") ) {
	    	return false; 
	    }
	    
		// Throws exception if it's already in the enumeration.
		if( literals.contains( literal ) ){
			throw new Exception("LiteralAlreadyExists!"); 
		}
		for( String elem : literals ){
			if( literal == elem ){
				throw new Exception("LiteralNameAlreadyExists!");
			}
		}

		literals.add( literal );
		
		return true;
	}
}


/*
 *"Eu sofro quando eu vejo a decad�ncia desse mundo injusto e imundo,
 * cheio de guerras e gente f�til." [CBJr]
 * 
 */
