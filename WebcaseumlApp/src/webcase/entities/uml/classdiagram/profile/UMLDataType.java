package webcase.entities.uml.classdiagram.profile;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.user.client.Window;

import webcase.entities.uml.TreatAttributes;
import webcase.entities.uml.TreatOperations;
import webcase.entities.uml.UMLAttribute;
import webcase.entities.uml.UMLOperation;
import webcase.entities.uml.standard.UMLStandardProfile;

/**
 * Entity of the element UMLDataType.
 * Contains its properties.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLDataType extends UMLProfileElement implements TreatAttributes, TreatOperations {
	private boolean isAbstract;
	private List<UMLAttribute> attributes;
	private List<UMLOperation> operations;
	
	/**
      * Default constructor.
      * Sets the data type name.
      * Sets the data type standard stereotype.
      * Sets the data type as non-abstract.
      * @param Data type name. 
	  */		
	public UMLDataType(String name) {
		try {
			// Apply the stereotype 'DataType' to this element
			applyStereotype( UMLStandardProfile.getStereotypeByName( "DataType" ) );
		} catch (Exception e) {
			Window.alert("Data Type Constructor Alert!\n" +
						"Couldn't apply stererotype to 'UMLDataType' element.\n" +
						"Element name: '" + name + "'");
			e.printStackTrace();
		} finally {
			setName( name );
			isAbstract = false;
			attributes = null;
			operations = null;
		}
	}

	/**
      * Returns if the data is abstract.
	  * @return Returns true if the data type is abstract
	  * 		or false otherwise.
	  */	
	public boolean isAbstract() {
		return isAbstract;
	}

	/**
      * Returns a list of the data type attributes.
	  * @return List of UML attributes.
	  * @throws	Thorws exception if there is not attributes
	  * 		in the data type.
	  */
	@Override
	public List<UMLAttribute> getAttributes() throws Exception {
		if ( attributes == null || attributes.isEmpty() ){
			throw new Exception("ThereIsNoAttribute!");
		}
		return attributes;
	}
	
	/**
      * Returns the data type list of operations.
	  * @return List of operations.
	  * @throws	Throws exception if there is no operation
	  * 		in the data type (ThereIsNoOperation!).
	  */
	@Override
	public List<UMLOperation> getOperations() throws Exception {
		if ( operations == null || operations.isEmpty() ){
			throw new Exception("ThereIsNoOperation!");
		}
		return operations;
	}		
	
	/**
      * Sets if the data types is abstract.
      * @param True or False.
	  */
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
      * Adds an attribute to the data type.
      * @param Attribute to be added.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the data type already has the
      * 		attribute to be added (AttributeAlreadyExists!).
      * @throws Throws exception if the data type already has an 
      * 		attribute with its name (AttributeNameAlreadyExists!).
	  */
	@Override
	public boolean addAttribute(UMLAttribute attr) throws Exception {
		if ( attributes == null ){
			attributes = new ArrayList<UMLAttribute>();
		}
		
		// Throws exception if it's already in the data type.
		if( attributes.contains( attr ) ){
			throw new Exception("AttributeAlreadyExists!"); 
		}
		for( UMLAttribute elem : attributes ){
			if( attr.getName() == elem.getName() ){
				throw new Exception("AttributeNameAlreadyExists!");
			}
		}
		
		attributes.add( attr );
		
		return true;
	}	

	/**
      * Adds an operation to the data type.
      * @param Operation to be added.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the data type already has the
      * 		operation to be added (OperationAlreadyExists!).
      * @throws Throws exception if the data type already has an 
      * 		operation with its name (OperationNameAlreadyExists!).
	  */
	@Override
	public boolean addOperation(UMLOperation operation) throws Exception {
		if ( operations == null ){
			operations = new ArrayList<UMLOperation>();
		}
		
		// Throws exception if it's already in the data type.
		if( operations.contains( operation ) ){
			throw new Exception("OperationAlreadyExists!");
		}
		for( UMLOperation elem : operations ){
			if( operation.getName() == elem.getName() ){
				throw new Exception("OperationNameAlreadyExists!");
			}
		}
		operations.add( operation );
		
		return true;
	}
	
	/**
      * Removes an attribute of this data type.
      * @param Attribute to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the data type there is no
      * 		attribute (ThereIsNotAttributes!).
      * @throws	Throws exception if the data type there is not the
      * 		attribute to be removed (ThereIsNotSuchAttribute!).
	  */
	@Override
	public boolean removeAttribute(UMLAttribute attr) throws Exception {
		if ( attributes == null || attributes.isEmpty() ){
			throw new Exception("ThereIsNotAttributes!");
		}
		else if ( !attributes.remove( attr ) ){
			throw new Exception("ThereIsNotSuchAttribute!");
		}
		return true;
	}

	/**
      * Removes an operation of this data type.
      * @param Operation to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if this data type there is no  
      * 		operations (ThereIsNotOperations!).
      * @throws	Throws exception if this data type there is not the
      * 		operation to be removed (ThereIsNotSuchOperation!).
	  */
	@Override
	public boolean removeOperation(UMLOperation op) throws Exception {
		if ( operations == null || operations.isEmpty() ){
			throw new Exception("ThereIsNotOperations!");
		}
		else if ( !operations.remove( op ) ){
			throw new Exception("ThereIsNotSuchOperation!");
		}
		return true;
	}
	
	/**
      * Returns the number of attributes of this data type.
      * @return Number of attributes.
      */
	@Override
	public int numberOfAttributes(){
		return attributes.size();
	}
	
	/**
     * Returns the number of operations of this data type.
     * @return Number of operations.
     */
	@Override
	public int numberOfOperations(){
		return operations.size();
	}
	
	/**
	  * Returns the attribute named as 'name'.
	  * @return UML attribute.
	  * @throws Throws exception if the data type has not an attribute
	  * 		named as 'name' (ThereIsNotSuchAttribute!).
	  */
	@Override
	public UMLAttribute getAttributeByName(String name) throws Exception{
		for( UMLAttribute elem : attributes ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchAttribute!");
	}
	
	/**
	  * Returns the operation named as 'name'.
	  * @return UML operation.
	  * @throws Throws exception if the data type has not an operation
	  * 		named as 'name' (ThereIsNotSuchOperation!).
	  */
	@Override
	public UMLOperation getOperationByName(String name) throws Exception{
		for( UMLOperation elem : operations ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchOperation!");
	}
}
