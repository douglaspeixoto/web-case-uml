package webcase.entities.uml.standard;

import java.util.ArrayList;

import webcase.entities.uml.classdiagram.profile.UMLDataType;

/**
 * Entity of the standard data type of this program.
 * Here we create a instance of the main UML data types
 * which can be used in the Webcaseuml application.
 * 
 * @author Douglas Alves Peixoto
 */
public final class UMLPrimiteTypes {
	private static final ArrayList<UMLDataType> standardDataTypes = 
		initializeDataTypes();
	
	/**
	 * Creates the program standard DataTypes.
	 */
	private static final ArrayList<UMLDataType> initializeDataTypes() {
		ArrayList<UMLDataType> list = new ArrayList<UMLDataType>();
		UMLDataType dataType;
		
		dataType = new UMLDataType("Integer");
		list.add( dataType );
		dataType = new UMLDataType("String");
		list.add( dataType );
		dataType = new UMLDataType("Boolean");
		list.add( dataType );
		dataType = new UMLDataType("Double");
		list.add( dataType );
		dataType = new UMLDataType("Float");
		list.add( dataType );
		dataType = new UMLDataType("Byte"); // CHAR
		list.add( dataType );
		dataType = new UMLDataType("Enumeration");
		list.add( dataType );
		
		/*declaration of others UML primitive data types*/
		
		return list;
	}

	/**
	  * Returns a list of standard UML data types
	  * (Integer, String, Enumeration, etc).
	  * @return List of data types. 
	  */
	public static final ArrayList<UMLDataType> getUMLStandardDataTypes(){
		return standardDataTypes;
	}
	
}
