package webcase.entities.uml.standard;

import java.util.ArrayList;
import java.util.List;

import webcase.entities.uml.classdiagram.profile.UMLMetaclass;
import webcase.entities.uml.classdiagram.profile.UMLProfile;
import webcase.entities.uml.classdiagram.profile.UMLProfileElement;
import webcase.entities.uml.classdiagram.profile.UMLStereotype;

/**
 * Entity of the standard profile of this program.
 * Contains the standard UML stereotypes.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLStandardProfile {
	private static final UMLProfile standardProfile = 
		initializeProfile();
	
	/**
	  * Creates the standard profile UMLStandardProfile.
	  */
	private static final UMLProfile initializeProfile() {
		UMLProfile profile = new UMLProfile("UMLStandardProfile");
		ArrayList<UMLProfileElement> list = new ArrayList<UMLProfileElement>();
		UMLStereotype stereotype;
		
		stereotype = new UMLStereotype("Stereotype");
		stereotype.setExtendedMetaclass( UMLMetaclass.UMLStereotype );
		list.add( stereotype );
		
		stereotype = new UMLStereotype("DataType");
		stereotype.setExtendedMetaclass( UMLMetaclass.UMLDataType );
		list.add( stereotype );
		
		stereotype = new UMLStereotype("Enumeration");
		stereotype.setExtendedMetaclass( UMLMetaclass.UMLEnumeration );
		list.add( stereotype );
		
		stereotype = new UMLStereotype("Interface");
		stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
		list.add( stereotype );
		
		stereotype = new UMLStereotype("Utility");
		stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
		list.add( stereotype );
		
		/*declaration of others standard stereotypes*/
		
		profile.setProfileElements( list );
		
		return profile;
	}

	/**
	  * Returns the profile which contains the standard
	  * UML stereotypes.
	  * @return UML Profile.
	  */
	public static final UMLProfile getUMLStandardProfile() {
		return standardProfile;
	}
	
	/**
	  * Returns the stereotypes from the standard UML profile .
	  * @return List of UML stereotypes.
	  */
	public static final List<UMLStereotype> getStereotypes() throws Exception {
		return standardProfile.getStereotypes();
	}
	
	/**
	  * Returns a stereotype from the profile by its name.
	  * @param Stereotype name to be searched.
	  * @return UML stereotype.
	 * @throws Exception 
	  */
	public static final UMLStereotype getStereotypeByName(String strName) throws Exception {
		return standardProfile.getStereotypeByName( strName );
	}

	/**
      * Returns the number of stereotypes from the profile.
      * @return Number of stereotypes from the profile.
      */
	public static final int numberOfStereotypes() {
		return standardProfile.numberOfStereotypes();
	}		
}
