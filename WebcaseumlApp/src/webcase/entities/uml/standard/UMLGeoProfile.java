package webcase.entities.uml.standard;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.user.client.Window;

import webcase.entities.uml.UMLAttribute;
import webcase.entities.uml.classdiagram.profile.UMLEnumeration;
import webcase.entities.uml.classdiagram.profile.UMLMetaclass;
import webcase.entities.uml.classdiagram.profile.UMLProfile;
import webcase.entities.uml.classdiagram.profile.UMLProfileElement;
import webcase.entities.uml.classdiagram.profile.UMLStereotype;

/**
  * Entity to gather the GeoProfile elements,
  * such as its stereotypes and tagged values. 
  * This program allows the creation of new 
  * profiles, but already has the GeoProfile 
  * implanted as a standard profile.
  * 
  * @author Douglas Alves Peixoto
  */
public class UMLGeoProfile {
	private static final UMLProfile geoProfile = 
		initializeGeoProfile();

	/**
	  * Initialize the GeoProfile.
	  * Creates its elements.
	  */
	private static final UMLProfile initializeGeoProfile() {
		UMLProfile profile = new UMLProfile("GeoProfile");
		ArrayList<UMLProfileElement> elements = new ArrayList<UMLProfileElement>();
		
		elements.addAll( getGeoObjects() );  
		elements.addAll( getGeoFields() );
		elements.addAll( getNetworkObjs() );
		elements.addAll( getAssociations() );
		elements.addAll( getTemporalObjs() );
		
		profile.setProfileElements( elements );
		
		return profile;
	}

	/**
	  * Creates the GeoProfile GeoObjects stereotypes.
	  * @return GeoObject elements.
	  */
	private static final List<UMLProfileElement> getGeoObjects(){
		ArrayList<UMLProfileElement> elements = 
			new ArrayList<UMLProfileElement>();
		UMLStereotype stereotype;
		UMLStereotype geoObject;
		
		try {
			/* Creates the GeoObject stereotypes */
			geoObject = new UMLStereotype("GeoObject");
			geoObject.setExtendedMetaclass( UMLMetaclass.UMLClass );
			geoObject.setAbstract( true );
			
			stereotype = new UMLStereotype("Point");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoObject );
			geoObject.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("Line");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoObject );
			geoObject.addSubclass( stereotype );
			elements.add( stereotype );
		
			stereotype = new UMLStereotype("Polygon");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoObject );
			geoObject.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("ComplexSpatialObj");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoObject );
			geoObject.addSubclass( stereotype );
			elements.add( stereotype );
			
			elements.add( geoObject );
			
		} catch (Exception e) {
			Window.alert("GeoProfile Alert!.\n" +
						"Couldn't create GeoProfile GeoObjects.");
			e.printStackTrace();
		}

		return elements;
	}
	
	/**
	  * Creates the GeoProfile GeoField stereotypes.
	  * @return GeoFields elements.
	  */
	private static final List<UMLProfileElement> getGeoFields(){
		ArrayList<UMLProfileElement> elements = 
			new ArrayList<UMLProfileElement>();
		UMLStereotype stereotype;
		UMLStereotype geoField;
		
		try {
			/* Creates the GeoField stereotypes */
			geoField = new UMLStereotype("GeoField");
			geoField.setExtendedMetaclass( UMLMetaclass.UMLClass );
			geoField.setAbstract( true );

			stereotype = new UMLStereotype("TIN");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoField );
			geoField.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("IrregularPoints");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoField );
			geoField.addSubclass( stereotype );
			elements.add( stereotype );
		
			stereotype = new UMLStereotype("Isolines");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoField );
			geoField.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("GridOfPoints");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoField );
			geoField.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("GridOfCells");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoField );
			geoField.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("AdjPolygons");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( geoField );
			geoField.addSubclass( stereotype );
			elements.add( stereotype );
			
			elements.add( geoField );
			
		} catch (Exception e) {
			Window.alert("GeoProfile Alert!.\n" +
						"Couldn't create GeoProfile GeoField objects.");
			e.printStackTrace();
		}

		return elements;
	}

	/**
	  * Creates the GeoProfile NetworkObj stereotypes.
	  * @return NetworkObj elements.
	  */
	private static final List<UMLProfileElement> getNetworkObjs(){
		ArrayList<UMLProfileElement> elements = 
			new ArrayList<UMLProfileElement>();
		UMLStereotype stereotype;
		UMLStereotype networkObj;
		UMLStereotype arc;
		
		try {
			/* Creates the Network stereotype */
			stereotype = new UMLStereotype("Network");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			elements.add( stereotype );
			
			/* Creates the NetworkObj stereotypes */
			networkObj = new UMLStereotype("NetworkObj");
			networkObj.setExtendedMetaclass( UMLMetaclass.UMLClass );
			networkObj.setAbstract( true );
			
			arc = new UMLStereotype("Arc");
			arc.setExtendedMetaclass( UMLMetaclass.UMLClass );
			arc.setAbstract( true );
			networkObj.addSubclass( arc );
			
			stereotype = new UMLStereotype("Node");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( networkObj );
			networkObj.addSubclass( stereotype );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("UnidirectionalArc");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( arc );
			arc.addSubclass( stereotype );
			elements.add( stereotype );
		
			stereotype = new UMLStereotype("BidirectionalArc");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLClass );
			stereotype.addSuperclass( arc );
			arc.addSubclass( stereotype );
			elements.add( stereotype );
			
			elements.add( arc );
			elements.add( networkObj );
			
		} catch (Exception e) {
			Window.alert("GeoProfile Alert!.\n" +
						"Couldn't create GeoProfile NetworkObj objects.");
			e.printStackTrace();
		}
		
		return elements;
	}

	/**
	  * Creates the GeoProfile Association stereotypes.
	  * @return Association elements.
	  */
	private static final List<UMLProfileElement> getAssociations(){
		ArrayList<UMLProfileElement> elements = new ArrayList<UMLProfileElement>();
		UMLStereotype stereotype;
		
		try {
			/* Creates the Association stereotypes */	
			stereotype = new UMLStereotype("Temporal");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLRelationship );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("In");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLRelationship );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("Cross");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLRelationship );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("Touch");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLRelationship );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("Disjoint");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLRelationship );
			elements.add( stereotype );
			
			stereotype = new UMLStereotype("Overlap");
			stereotype.setExtendedMetaclass( UMLMetaclass.UMLRelationship );
			elements.add( stereotype );
						
		} catch (Exception e) {
			Window.alert("GeoProfile Alert!.\n" +
						"Couldn't create GeoProfile Associations.");
			e.printStackTrace();
		}

		return elements;
	}
	
	/**
	  * Creates the GeoProfile TemporalObj elements.
	  * @return TemporalObj elements.
	  */
	private static final List<UMLProfileElement> getTemporalObjs() {
		ArrayList<UMLProfileElement> elements = new ArrayList<UMLProfileElement>();
		UMLStereotype temporalObj;
		UMLEnumeration temporalPrimitive;
		UMLEnumeration temporalType;
		UMLAttribute tValuePrimitive;
		UMLAttribute tValueType;
		
		try {
			// Creates the GeoProfile enumerations.
			temporalPrimitive = new UMLEnumeration("TemporalPrimitive");
			temporalPrimitive.addLiteral("instant");
			temporalPrimitive.addLiteral("interval");
			elements.add(temporalPrimitive);
			
			temporalType = new UMLEnumeration("TemporalType");
			temporalType.addLiteral("valid_time");
			temporalType.addLiteral("transaction_time");
			temporalType.addLiteral("bitemporal");
			elements.add(temporalType);
		
			// Creates TemporalObj attributes.
			tValuePrimitive = new UMLAttribute("temporalPrimitive");
			tValuePrimitive.setType( temporalPrimitive );
			
			tValueType = new UMLAttribute("temporalType");
			tValueType.setType( temporalType );
			
			// Creates the stereotype TemporalObj.
			temporalObj = new UMLStereotype("TemporalObj");
			temporalObj.setExtendedMetaclass( UMLMetaclass.UMLClass );
			temporalObj.addAttribute( tValuePrimitive );
			temporalObj.addAttribute( tValueType );
			elements.add( temporalObj );
			
		} catch (Exception e) {
			Window.alert("GeoProfile Alert!.\n" +
						"Couldn't create GeoProfile TemporalObj.");
			e.printStackTrace();
		}

		return elements;
	}
	
	/**
	  * Returns the GeoProfile, with its elements.
	  * @return UML profile (GeoProfile).
	  */
	public static final UMLProfile getGeoProfile() {
		return geoProfile;
	}

	/**
	  * Returns the GeoProfile stereotypes.
	  * @return UML stereotypes.
	  */
	public static final List<UMLStereotype> getStereotypes() throws Exception {
		return geoProfile.getStereotypes();
	}

	/**
	  * Returns a GeoProfile stereotype by its name.
	  * @param Stereotype name to be searched.
	  * @return UML stereotype.
	  */
	public static final UMLStereotype getStereotypeByName(String strName) throws Exception {
		return geoProfile.getStereotypeByName( strName );
	}

	/**
      * Returns the number of stereotypes from the GeoProfile.
      * @return Number of stereotypes from the GeoProfile.
      */
	public static final int numberOfStereotypes() {
		return geoProfile.numberOfStereotypes();
	}

	/*
	 * Call the method that validates the schema
	 * (OCL constraints)
	 */
}
