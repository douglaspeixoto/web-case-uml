package webcase.entities.uml;

import java.util.ArrayList;
import java.util.List;

import webcase.entities.uml.TreatAttributes;
import webcase.entities.uml.classdiagram.profile.UMLVisibility;

/**
 * Entity of the element UMLOperation.
 * Contains its properties.
 * 
 * @author Douglas Alves Peixoto
 */
public class UMLOperation extends UMLElement implements TreatAttributes {
	private UMLElement returnType;
	private List<UMLAttribute> parameters;
	private boolean isAbstract;
	private boolean isStatic;
	private UMLVisibility visibility;

	/**
      * Default constructor.
      * Sets the operation name.
	  * Sets the operation as non-static.
	  * Sets the visibility as PUBLIC.
	  * @param Operation name.
	  */	
	public UMLOperation(String name) {
		setName( name );
		parameters = null;
		isAbstract = false;
		isStatic = false;
		visibility = UMLVisibility.PUBLIC;
	}

	/**
      * Returns the return type of the operation.
	  * @return UML element.
	  */
	public UMLElement getReturnType() {
		return returnType;
	}

	/**
      * Returns the operation return type.
	  * @return Name of the return type.
	  */
	public String getReturnTypeName() {
		return returnType.getName();
	}
	
	/**
      * Returns a list of parameters from the operation.
	  * @return List of parameters (UML attributes).
	  * @throws	Throws exception if there is not parameters
	  * 		in the operation (ThereIsNoParameter!).
	  */
	@Override
	public List<UMLAttribute> getAttributes() throws Exception {
		if ( parameters == null || parameters.isEmpty() ){
			throw new Exception("ThereIsNoParameter!"); 
		}
		return parameters;
	}

	/**
      * Returns if the operation is abstract.
	  * @return Returns true if the operation is abstract, 
	  * 		false otherwise.
	  */
	public boolean isAbstract() {
		return isAbstract;
	}

	/**
      * Returns if the operation is static.
	  * @return Returns true if the operation is static, 
	  * 		false otherwise.
	  */
	public boolean isStatic() {
		return isStatic;
	}

	/**
      * Returns the operation visibility.
	  * @return Enumeration, can be PUBLIC, PRIVATE or PROTECTED.
	  */
	public UMLVisibility getVisibility() {
		return visibility;
	}

	/**
      * Sets the return type of the operation.
      * @param UML element.
	  */
	public void setReturnType(UMLElement returnType) {
		this.returnType = returnType;
	}

	/**
      * Adds a new parameter in the operation.
      * @param Parameter (UML attribute) to be added.
      * @return Returns true if it has been successfully added.
	  * @throws Throws exception if the operations already has the parameter
      * 		(attribute) to be added (ParameterAlreadyExists!).
	  * @throws Throws exception if the operation already has a parameter 
      * 		(attribute) with this name (ParameterNameAlreadyExists!).
	  */
	@Override
	public boolean addAttribute(UMLAttribute param) throws Exception {
		if ( parameters == null ){
			parameters = new ArrayList<UMLAttribute>();
		}
		
		// Throws exception if it's already in the operation.
		if( parameters.contains( param ) ){
			throw new Exception("ParameterAlreadyExists!");
		}
		for( UMLAttribute elem : parameters ){
			if( param.getName() == elem.getName() ){
				throw new Exception("ParameterNameAlreadyExists!");
			}
		}
		
		parameters.add( param );
		
		return true;
	}

	/**
      * Sets if the operation is abstract.
      * @param True or False
	  */
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
      * Sets if the operation is static
      * @param True or False
	  */
	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}

	/**
      * Sets the operation visibility.
      * @param Enumeration (PUBLIC, PRIVATE or PROTECTED)
	  */
	public void setVisibility(UMLVisibility visibility) {
		this.visibility = visibility;
	}

	/**
      * Removes an operation parameter.
      * @param Paremter (UML attribute) to be removed.
      * @return Returns true if it has been successfully removed.
      * @throws	Throws exception if the operations has not  
      * 		parameters (ThereIsNotParameters!).
      * @throws	Throws exception if the operations has not the
      * 		the parameter to be deleted (ThereIsNotSuchParameter!).
	  */	
	@Override
	public boolean removeAttribute(UMLAttribute param) throws Exception {
		if ( parameters == null || parameters.isEmpty() ){
			throw new Exception("ThereIsNotParameters!");
		}
		else if ( !parameters.remove( param ) ){
			throw new Exception("ThereIsNotSuchParameter!"); 
		}
		return true;
	}
	
	/**
      * Returns the number of parameter (UML attributes) 
      * from this operation.
      * @return Number of parameters.
      */
	@Override
	public int numberOfAttributes(){
		return parameters.size();
	}
	
	/**
	  * Returns the parameter named as 'name'.
	  * @return UML attribute.
	  * @throws Throws exception if the operation has not a parameter
	  * 		named as 'name' (ThereIsNotSuchPrameter!).
	  */
	@Override
	public UMLAttribute getAttributeByName(String name) throws Exception {
		for( UMLAttribute elem : parameters ){
			if( elem.getName() == name ){
				return elem;
			}
		}
		throw new Exception("ThereIsNotSuchParameter!");
	}
}


/*
 *"Uma alma carregada de �dio e amor,
 * mas muito mais �dio que amor." 
 */